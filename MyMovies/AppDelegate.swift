import UIKit
import Swinject

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let container = Container()
        
        // Storages
        container.register(UpcomingMoviesStorage.self) { r in
            UpcomingMoviesStorage()
        }.inObjectScope(.container)
        
        container.register(PopularMoviesStorage.self) { r in
            PopularMoviesStorage()
        }.inObjectScope(.container)
        
        container.register(TopRatedMoviesStorage.self) { r in
            TopRatedMoviesStorage()
        }.inObjectScope(.container)
        
        container.register(SearcherStorage.self) { r in
            SearcherStorage()
        }.inObjectScope(.container)
        
        container.register(MyMoviesZoneStorage.self) { r in
            MyMoviesZoneStorage()
        }.inObjectScope(.container)
        
        container.register(UpcomingMoviesStorage.self) { r in
            UpcomingMoviesStorage()
        }.inObjectScope(.container)
        
        // ViewModels
        
        container.register(HomeViewModel.self) { r in
            HomeViewModel(upcomingMoviesStorage: r.resolve(UpcomingMoviesStorage.self)!,
                          topRatedMoviesStorage: r.resolve(TopRatedMoviesStorage.self)!,
                          popularMoviesStorage: r.resolve(PopularMoviesStorage.self)!)
        }.inObjectScope(.container)
        
        container.register(SearcherViewModel.self) {r in
            SearcherViewModel(searcherStorage: r.resolve(SearcherStorage.self)!)
        }.inObjectScope(.container)
        
        container.register(MyMoviesZoneViewModel.self) {r in
            MyMoviesZoneViewModel(myMoviesZoneStorage: r.resolve(MyMoviesZoneStorage.self)!)
        }.inObjectScope(.container)

        // ViewControllers
        
        container.register(HomeViewController.self) {r in
            let home = HomeViewController(homeViewModel: r.resolve(HomeViewModel.self)!)
            home.tabBarItem.title = ConstantString.tabBarTitles.home
            home.tabBarItem.image = UIImage(named: ConstantString.imagesNames.home)
            return home
        }.inObjectScope(.container)
        
        container.register(MyMoviesZoneViewController.self) {r in
            let myMoviesZone = MyMoviesZoneViewController(myMoviesZoneViewModel: r.resolve(MyMoviesZoneViewModel.self)!)
            myMoviesZone.tabBarItem.title = ConstantString.tabBarTitles.home
            myMoviesZone.tabBarItem.image = UIImage(named: ConstantString.imagesNames.myMoviesZone)
            return myMoviesZone
        }.inObjectScope(.container)
        
        container.register(SearcherViewController.self) {r in
            let searcherViewController = SearcherViewController(searcherViewModel: r.resolve(SearcherViewModel.self)!)
            searcherViewController.tabBarItem.title = ConstantString.tabBarTitles.searcher
            searcherViewController.tabBarItem.image = UIImage(named: ConstantString.imagesNames.search)
            return searcherViewController
        }.inObjectScope(.container)

        // Tab bar controller
        
        container.register(UITabBarController.self) {r in
            let tbc = UITabBarController()
            tbc.tabBar.backgroundColor = UIColor.black
            tbc.tabBar.tintColor = UIColor.black
            
            tbc.setViewControllers([r.resolve(HomeViewController.self)!,
                                    r.resolve(MyMoviesZoneViewController.self)!,
                                    r.resolve(SearcherViewController.self)!],
                                   animated: true)
            return tbc
        }.inObjectScope(.container)
        
        container.register(UINavigationController.self) {r in
            UINavigationController(rootViewController: r.resolve(UITabBarController.self)!)
        }.inObjectScope(.container)
        
        window = UIWindow(frame: UIScreen.main.bounds)

        window?.rootViewController = container.resolve(UINavigationController.self)!

        window?.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

