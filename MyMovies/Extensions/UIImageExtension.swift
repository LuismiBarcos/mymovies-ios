import UIKit

extension UIImage {
    /**
     * https://stackoverflow.com/questions/44780937/storing-and-retrieving-image-in-sqlite-with-swift
     * https://stackoverflow.com/questions/11251340/convert-between-uiimage-and-base64-string
     */

    func encodeImageToBase64() -> String {
        let imageData: NSData = UIImageJPEGRepresentation(self, 100)! as NSData
        return imageData.base64EncodedString(options: .lineLength64Characters)
    }
}
