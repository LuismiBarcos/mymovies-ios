import UIKit

extension String {
    
    /**
    * https://stackoverflow.com/questions/32851720/how-to-remove-special-characters-from-string-in-swift-2
    */
    var removeSpecialChars: String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+")
        return self.filter {okayChars.contains($0) }
    }
    
    /**
     * https://stackoverflow.com/questions/44780937/storing-and-retrieving-image-in-sqlite-with-swift
     * https://stackoverflow.com/questions/11251340/convert-between-uiimage-and-base64-string
     */
    func decodeBase64ToImage() -> UIImage{
        let dataDecoded : Data = Data(base64Encoded: self, options: .ignoreUnknownCharacters)!
        return UIImage(data: dataDecoded as Data)!
    }
}
