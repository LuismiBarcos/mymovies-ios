import UIKit

extension UIViewController {
    
    func startSpinner(activityIndicator: UIActivityIndicatorView){
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    func stopSpinner(activityIndicator: UIActivityIndicatorView){
        activityIndicator.stopAnimating()
    }
}
