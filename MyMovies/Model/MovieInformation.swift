import UIKit
import SQLite

class MovieInformation {
    let id: Int
    var title: String?
    var genres: [String]?
    var overview: String?
    var average: Float?
    var voteCount: Int?
    var releaseDate: String?
    var coverImage: UIImage?

    let conn: Connection
    
    private let table = Table("MovieInformation")
    
    private let idField = Expression<Int>("id")
    private let titleField = Expression<String>("title")
    private let genresField = Expression<String>("genres")
    private let overviewField = Expression<String>("overview")
    private let averageField = Expression<Double>("average")
    private let voteCountField = Expression<Int>("voteCount")
    private let releaseDateField = Expression<String>("releaseDate")
    private var coverImageField = Expression<String>("coverImage")
    
    init(conn: Connection, movieId: Int) {
        self.conn = conn
        self.id = movieId
        self.initValues(movieId: movieId)
    }
    
    init(conn: Connection, id: Int, title: String, genres:[String], voteCount: Int,
         overview: String, average: Float, releaseDate: String) {
        self.conn = conn
        self.id = id
        self.title = title
        self.genres = genres
        self.overview = overview
        self.average = average
        self.voteCount = voteCount
        self.releaseDate = releaseDate
        self.coverImage = UIImage(named: ConstantString.imagesNames.movieNotAvailable)!
    }
    
    private func initValues(movieId: Int){
        let query = table.where(idField == self.id)
        let results = try! conn.prepare(query)
        for result in results {
            self.title = result[titleField]
            self.genres = self.decodeJsonGenres(result[genresField])
            self.overview = result[overviewField]
            self.average = Float(result[averageField])
            self.voteCount = result[voteCountField]
            self.releaseDate = result[releaseDateField]
            self.coverImage = result[coverImageField].decodeBase64ToImage()
        }
    }
    
    func createTable(){
        do {
            let statement = table.create(temporary: false, ifNotExists: true){ t in
                t.column(idField, primaryKey: true)
                t.column(titleField)
                t.column(genresField)
                t.column(overviewField)
                t.column(averageField)
                t.column(voteCountField)
                t.column(releaseDateField)
                t.column(coverImageField)
            }
            try conn.run(statement)
        } catch {
            print("Error al crear la tabla")
        }
    }
    
    func getMovieById(movieId: Int) ->MovieInformation {
        let query = table.where(idField == self.id)
        let results = try! conn.prepare(query)
        var movie: MovieInformation?
        for result in results {
            movie = MovieInformation(conn: self.conn,
                                     id: self.id,
                                     title: result[titleField],
                                     genres: self.decodeJsonGenres(result[genresField]),
                                     voteCount: result[voteCountField],
                                     overview: result[overviewField],
                                     average: Float(result[averageField]),
                                     releaseDate: result[releaseDateField])
            movie?.coverImage = result[coverImageField].decodeBase64ToImage()
        }
        return movie!
    }
    
    func save(){
        if !exists() {
            try! self.insert()
        }
    }
    
    private func insert() throws {
        let genres = self.encodeGenresToJson()
        do {
            let insert = table.insert(
                idField <- self.id,
                titleField <- self.title!,
                genresField <- genres,
                overviewField <- self.overview!,
                averageField <- Double(self.average!),
                voteCountField <- self.voteCount!,
                releaseDateField <- self.releaseDate!,
                coverImageField <- self.coverImage!.encodeImageToBase64()
            )
            try conn.run(insert)
        } catch {
            print("Error al insertar la pelicula")
        }
    }
    
    func delete(){
        let movieToDelete = table.where(idField == self.id)
        try! conn.run(movieToDelete.delete())
    }
    
    private func exists() -> Bool {
        var exists = false
        let query = table.where(idField == self.id)
        let results = try! conn.prepare(query)
        for _ in results {
            exists = true
        }
        return exists
    }
    
    private func encodeGenresToJson() -> String{
        guard let json = try? JSONSerialization.data(withJSONObject: self.genres!, options: []) else {
            print("Error convirtiendo generos en json")
            return ""
        }
        return String(data: json, encoding: String.Encoding.utf8)!
    }
    
    private func decodeJsonGenres(_ json: String) -> [String]{
        let data = json.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        return try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String]
    }
}
