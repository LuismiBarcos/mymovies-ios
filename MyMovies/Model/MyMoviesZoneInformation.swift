import UIKit
import SQLite

class MyMoviesZoneInformation {
    let movieId: Int
    var favorite: Bool = false
    var viewed: Bool = false
    var pending: Bool = false
    
    let conn: Connection
    
    private let table = Table("MyMoviesZoneInformation")
    
    private let movieIdField = Expression<Int>("movieId")
    private let isFavoriteField = Expression<Bool>("favorite")
    private let isViewedField = Expression<Bool>("viewed")
    private let isPendingField = Expression<Bool>("pending")
    
    init(movieId: Int, conn: Connection) {
        self.movieId = movieId
        self.conn = conn
        self.createTable()
        self.initValues(movieId: movieId)
    }
    
    private func initValues(movieId: Int){
        let query = table.where(movieIdField == movieId)
        let results = try! conn.prepare(query)
        for result in results {
            self.viewed = result[isViewedField]
            self.favorite = result[isFavoriteField]
            self.pending = result[isPendingField]
        }
    }
    
    func createTable(){
        do {
            let statement = table.create(temporary: false, ifNotExists: true) { t in
                t.column(movieIdField, primaryKey: true)
                t.column(isFavoriteField)
                t.column(isViewedField)
                t.column(isPendingField)
            }
            try conn.run(statement)
        } catch {
            print("Error al crear la tabla")
        }
    }
    
    func getAllFavoriteMoviesIds() -> [Int]{
        let query = table.select(movieIdField)
                    .filter(isFavoriteField == true)
        return self.getAllMoviesIdByQuery(query)
    }
    
    func getAllViewedMoviesIds() -> [Int]{
        let query = table.select(movieIdField)
            .filter(isViewedField == true)
        return self.getAllMoviesIdByQuery(query)
    }
    
    func getAllPendingMoviesIds() -> [Int]{
        let query = table.select(movieIdField)
            .filter(isPendingField == true)
        return self.getAllMoviesIdByQuery(query)
    }
    
    private func getAllMoviesIdByQuery(_ query: Table) -> [Int]{
        let results = try! conn.prepare(query)
        var moviesIds: [Int] = []
        for result in results {
            moviesIds.append(result[movieIdField])
        }
        return moviesIds
    }
    
    func save(){
        (self.exists())
            ? self.update()
            : try! self.insert()
    }
    
    private func insert() throws {
        if self.favorite != false || self.viewed != false || self.pending != false{
            do {
                let insert = table.insert(
                    movieIdField <- self.movieId,
                    isFavoriteField <- self.favorite,
                    isViewedField <- self.viewed,
                    isPendingField <- self.pending)
                try conn.run(insert)
            } catch {
                print("Error al insertar en MyMoviesZoneInformation")
            }
        }
    }
    
    private func update(){
        if self.favorite != false || self.viewed != false || self.pending != false {
            do {
                let movieToUpdate = table.where(movieIdField == self.movieId)
                if try conn.run(movieToUpdate.update(
                    isFavoriteField <- self.favorite,
                    isPendingField <- self.pending,
                    isViewedField <- self.viewed)
                    ) > 0 {
                    print("updated movie \(self.movieId)")
                } else {
                    print("movie not found")
                }
            } catch {
                print("update failed: \(error)")
            }
        } else {
            self.delete()
        }
    }
    
    private func delete(){
        let movieToDelete = table.where(movieIdField == self.movieId)
        try! conn.run(movieToDelete.delete())
    }
    
    private func exists() -> Bool {
        var exists = false
        let query = table.where(movieIdField == self.movieId)
        let results = try! conn.prepare(query)
        for _ in results {
            exists = true
        }
        return exists
    }
}
