import UIKit

struct ConstantString {
    struct appSettings {
        static let apiKey = "5a17a96b0fd9c9075623cb996291044d"
        static let baseUrl = "https://api.themoviedb.org/3/"
        static let baseImageUrl = "https://image.tmdb.org/t/p/w500/"
        static let baseMoviesUrl = baseUrl + "movie/"
    }
    
    struct screensNibNames {
        static let searcher = "Searcher"
        static let searchByGenreResults = "SearchByGenreResults"
        static let searchByNameResults = "SearchByNameResults"
        static let myMoviesZone = "MyMoviesZone"
        static let movieMemberMoviesList = "MovieMemberMoviesList"
        static let movieMemberInformation = "MovieMemberInformation"
        static let movieTeamMembersList = "MovieTeamMembersList"
        static let movieInformation = "MovieInformation"
    }
    
    struct screensTitles {
        static let searchResults = "Results"
        static let searcher = "Searcher"
        static let myMoviesZone = "My Movies Zone"
        static let moviesList = "Movies"
        static let personalInformation = "Personal information"
        static let movieMembers = "Movie members"
        static let movieInformation = "Movie information"
        static let home = "Home"
    }
    
    struct imagesNames {
        static let home = "home"
        static let search = "search"
        static let myMoviesZone = "myMoviesZone"
        static let imageNotAvailable = "imageNotAvailable"
        static let movieNotAvailable = "movieNotAvailable"
    }
    
    struct tabBarTitles {
        static let home = "Home"
        static let myMoviesZone = "My movies zone"
        static let searcher = "Searcher"
    }
    
    struct defaultMessages {
        static let notAvailable = "Not available"
    }
    
    struct searcherConstants {
        static let sectionSearchByName = "Search by name"
        static let sectionSearchByGenre = "Search by genre"
        static let sectionByNamePeople = "People"
        static let sectionByNameMovies = "Movies"
    }
    
    struct movieInformationConstants {
        static let viewCast = "View cast"
        static let viewTeam = "View team"
    }
}

