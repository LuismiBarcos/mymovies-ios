import UIKit

protocol SearchByNameResultsStorageDelegate: class {
    func moviesResultsLoaded(searchByNameResultsStorage: SearchByNameResultsStorage)
    func peopleResultsLoaded(searchByNameResultsStorage: SearchByNameResultsStorage)
    func loadedImage()
}

class SearchByNameResultsStorage: ApiConnector {
    weak var delegate: SearchByNameResultsStorageDelegate?
    
    var allMoviesResult: [MovieResultInformation] = []
    var allPeopleResult: [PersonResultInformation] = []
    var searchingMovies = false
    
    func loadResultsByName(nameToSearch: String, searchMovie: Bool){
        self.searchingMovies = searchMovie
        let url = searchMovie ? self.generateMovieUrl(nameToSearch) : self.generatePersonUrl(nameToSearch)
        getJSONFromAPI(url: url)
    }
    
    private func generateMovieUrl(_ title: String) -> URL{
        self.allMoviesResult.removeAll()
        let url = "\(ConstantString.appSettings.baseUrl)search/movie?api_key=\(ConstantString.appSettings.apiKey)&query=\(title)"
        return URL(string: url)!
    }
    
    private func generatePersonUrl(_ name: String) -> URL{
        self.allPeopleResult.removeAll()
        let url = "\(ConstantString.appSettings.baseUrl)search/person?api_key=\(ConstantString.appSettings.apiKey)&query=\(name)"
        return URL(string: url)!
    }
    
    override func extractInformation(dataInJSON: [String : Any]) {
        (searchingMovies)
            ? self.extractMoviesResults(dataInJSON)
            : self.extractPeopleResults(dataInJSON)
    }
    
    // Getting movies
    
    private func extractMoviesResults(_ json: [String: Any]){
        guard let results = json["results"] as? [[String: Any]]
            else {
                print("Error searching movies by name")
                return
        }
        results.map { (movie) in
            self.allMoviesResult.append(self.extractMovieResult(movie))
        }
        DispatchQueue.main.async {
            self.delegate?.moviesResultsLoaded(searchByNameResultsStorage: self)
        }
    }
    
    private func extractMovieResult(_ movie: [String: Any]) -> MovieResultInformation{
        guard let id = movie["id"] as? Int,
            let title = movie["title"] as? String,
            let average = movie["vote_average"] as? Float
            else{
                print("Error getting information of movie result")
                return MovieResultInformation()
        }
        guard let imageUrl = movie["poster_path"] as? String
            else{
                return MovieResultInformation(id: id, title: title, voteAverage: average, imageUrl: "")
        }
        let movieResultInformation = MovieResultInformation(id: id, title: title, voteAverage: average, imageUrl: "\(ConstantString.appSettings.baseImageUrl)\(imageUrl)")
        self.downloadImage(object: movieResultInformation, url: movieResultInformation.imageUrl)
        return movieResultInformation
    }
    
    // Getting people
    
    private func extractPeopleResults(_ json: [String: Any]){
        guard let results = json["results"] as? [[String: Any]]
            else {
                print("Error searching movies by name")
                return
        }
        results.map { (person) in
            self.allPeopleResult.append(self.extractPersonResult(person))
        }
        DispatchQueue.main.async {
            self.delegate?.peopleResultsLoaded(searchByNameResultsStorage: self)
        }
    }
    
    private func extractPersonResult(_ person: [String: Any]) -> PersonResultInformation{
        guard let id = person["id"] as? Int,
            let name = person["name"] as? String
            else{
                print("Error getting information of movie result")
                return PersonResultInformation()
        }
        guard let imageUrl = person["profile_path"] as? String
            else{
                return PersonResultInformation(id: id, name: name, imageUrl: "")
        }
        let personResultInformation = PersonResultInformation(id: id, name: name, imageUrl: "\(ConstantString.appSettings.baseImageUrl)\(imageUrl)")
        self.downloadImage(object: personResultInformation, url: personResultInformation.imageUrl)
        return personResultInformation
    }
    
    override func saveImageInObject(object: Any, image: UIImage) {
        if type(of: object) == MovieResultInformation.self {
            let movieResultInformation = object as! MovieResultInformation
            movieResultInformation.image = image
        } else {
            let personResultInformation = object as! PersonResultInformation
            personResultInformation.image = image
        }
        self.notifyMainThread()
    }
    
    private func notifyMainThread(){
        DispatchQueue.main.async {
            self.delegate?.loadedImage()
        }
    }
}
