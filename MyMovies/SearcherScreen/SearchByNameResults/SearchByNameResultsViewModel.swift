import UIKit

protocol SearchByNameResultsViewModelDelegate: class {
    func resultsLoaded()
    func imageLoaded()
}

class SearchByNameResultsViewModel: SearchByNameResultsStorageDelegate {
    weak var delegate: SearchByNameResultsViewModelDelegate?
    
    var allMoviesResultsInformationViewModels: [MovieResultInformationViewModel] = []
    var allPersonResultsInformationViewModels: [PersonResultInformationViewModel] = []
    
    private let searchByNameResultsStorage: SearchByNameResultsStorage
    
    init(searchByNameResultsStorage: SearchByNameResultsStorage) {
        self.searchByNameResultsStorage = searchByNameResultsStorage
        self.searchByNameResultsStorage.delegate = self
    }
    
    func getMovieInformationViewModel() -> MovieInformationViewModel {
        let movieInformationStorage = MovieinformationStorage()
        return MovieInformationViewModel(movieInformationStorage: movieInformationStorage)
    }
    
    func getMovieMemberInformationViewModel() -> MovieMemberInformationViewModel {
        let movieMemberInformationStorage = MovieMemberInformationStorage()
        return MovieMemberInformationViewModel(movieMemberInformationStorage: movieMemberInformationStorage)
    }
    
    func loadResults(stringToSearch: String, searchMovies: Bool){
        let finalString = self.handleSpecialCharacters(stringToSearch)
        self.searchByNameResultsStorage.loadResultsByName(nameToSearch: finalString, searchMovie: searchMovies)
    }
    
    private func handleSpecialCharacters(_ string: String) -> String{
        let stringWithoutSpaces = string.replacingOccurrences(of: " ", with: "+")
        let stringWithoutDiacriticsCharacters = stringWithoutSpaces.folding(options: .diacriticInsensitive, locale: .current)
        let finalString = stringWithoutDiacriticsCharacters.removeSpecialChars
        return finalString
    }
    
    private func updateMoviesResultsViewModels(_ searchByNameResultsStorage: SearchByNameResultsStorage){
        allMoviesResultsInformationViewModels = searchByNameResultsStorage.allMoviesResult.map({ (movie) in
            return MovieResultInformationViewModel(movieResultInformation: movie)
        })
        self.notifyMainThread()
    }
    
    private func updatePersonResultsViewModels(_ searchByNameResultsStorage: SearchByNameResultsStorage){
        allPersonResultsInformationViewModels = searchByNameResultsStorage.allPeopleResult.map({ (person) in
            return PersonResultInformationViewModel(personResultInformation: person)
        })
        self.notifyMainThread()
    }
    
    private func notifyMainThread(){
        DispatchQueue.main.async {
            self.delegate?.resultsLoaded()
        }
    }
    
    // MARK SearchByNameResultsStorageDelegate methods
    func peopleResultsLoaded(searchByNameResultsStorage: SearchByNameResultsStorage) {
        self.updatePersonResultsViewModels(searchByNameResultsStorage)
    }
    
    func moviesResultsLoaded(searchByNameResultsStorage: SearchByNameResultsStorage) {
        self.updateMoviesResultsViewModels(searchByNameResultsStorage)
    }
    
    func loadedImage() {
        self.delegate?.imageLoaded()
    }
}
