import UIKit

class SearchByNameResultsViewController: UIViewController,
    UISearchBarDelegate,
    UITableViewDelegate,
    UITableViewDataSource,
    SearchByNameResultsViewModelDelegate
{
    private let MOVIE_CELL_IDENTIFIER = "movieCellView"
    
    private let searchByNameResultsViewModel: SearchByNameResultsViewModel
    private let searchMovies: Bool
    
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var resultsTableView: UITableView!
    
    init(searchMovies: Bool, searchByNameResultsViewModel: SearchByNameResultsViewModel) {
        self.searchByNameResultsViewModel = searchByNameResultsViewModel
        self.searchMovies = searchMovies
        super.init(nibName: ConstantString.screensNibNames.searchByNameResults, bundle: nil)
        self.searchByNameResultsViewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = ConstantString.screensTitles.searchResults
        self.resultsTableView.delegate = self
        self.resultsTableView.dataSource = self
        self.resultsTableView.register(MovieTableViewCell.self, forCellReuseIdentifier: MOVIE_CELL_IDENTIFIER)
        self.searchBar.delegate = self
        self.searchBar.returnKeyType = UIReturnKeyType.done
    }
    
    // MARK SearchByNameResultsViewModelDelegate methods
    
    func resultsLoaded() {
        self.resultsTableView.reloadData()
    }
    
    func imageLoaded() {
        self.resultsTableView.reloadData()
    }
    
    // MARK UISearchBarDelegate methods
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count >= 3 {
            self.searchByNameResultsViewModel.loadResults(stringToSearch: searchText, searchMovies: self.searchMovies)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.searchBar.endEditing(true)
    }
    
    // MARK UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.searchMovies)
            ? self.searchByNameResultsViewModel.allMoviesResultsInformationViewModels.count
            : self.searchByNameResultsViewModel.allPersonResultsInformationViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = resultsTableView.dequeueReusableCell(withIdentifier: MOVIE_CELL_IDENTIFIER, for: indexPath)
        if self.searchMovies {
            cell.textLabel?.text = self.searchByNameResultsViewModel.allMoviesResultsInformationViewModels[indexPath.row].movieTitle
            cell.imageView?.image = self.searchByNameResultsViewModel.allMoviesResultsInformationViewModels[indexPath.row].movieImage
        } else {
            cell.textLabel?.text = self.searchByNameResultsViewModel.allPersonResultsInformationViewModels[indexPath.row].personName
            cell.imageView?.image = self.searchByNameResultsViewModel.allPersonResultsInformationViewModels[indexPath.row].personImage
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var vc: UIViewController
        if self.searchMovies {
            let movieInformationViewModel = self.searchByNameResultsViewModel.getMovieInformationViewModel()
            let movieInformation = self.searchByNameResultsViewModel.allMoviesResultsInformationViewModels[indexPath.row]
            vc = MovieInformationViewController(movieId: movieInformation.movieId, movieCover: movieInformation.movieImage, movieInformationViewModel: movieInformationViewModel)
        } else {
            let movieMemberInformationViewModel = self.searchByNameResultsViewModel.getMovieMemberInformationViewModel()
            let personInformation = self.searchByNameResultsViewModel.allPersonResultsInformationViewModels[indexPath.row]
            vc = MovieMemberInformationViewController(memberId: personInformation.personId, memberImage: personInformation.personImage, movieMemberInformationViewModel: movieMemberInformationViewModel)
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
