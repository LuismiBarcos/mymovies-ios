import UIKit

class PersonResultInformation {
    let id: Int
    let name: String
    let imageUrl: String
    var image: UIImage
    
    init() {
        self.id = 0
        self.name = ConstantString.defaultMessages.notAvailable
        self.imageUrl = ""
        self.image = UIImage(named: ConstantString.imagesNames.imageNotAvailable)!
    }
    
    init(id: Int, name: String, imageUrl: String) {
        self.id = id
        self.name = name
        self.imageUrl = imageUrl
        self.image = UIImage(named: ConstantString.imagesNames.imageNotAvailable)!
    }
}
