import UIKit

class PersonResultInformationViewModel {
    let personResultInformation: PersonResultInformation
    
    init(personResultInformation: PersonResultInformation) {
        self.personResultInformation = personResultInformation
    }
    
    var personId: Int {
        get {
            return self.personResultInformation.id
        }
    }
    
    var personName: String {
        get {
            return self.personResultInformation.name
        }
    }
    
    var personImage: UIImage {
        get {
            return self.personResultInformation.image
        }
    }
}
