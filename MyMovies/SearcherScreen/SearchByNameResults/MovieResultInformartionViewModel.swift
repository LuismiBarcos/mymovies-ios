import UIKit

class MovieResultInformationViewModel {
    let movieResultInformation: MovieResultInformation
    
    init(movieResultInformation: MovieResultInformation) {
        self.movieResultInformation = movieResultInformation
    }
    
    var movieId: Int {
        get {
            return self.movieResultInformation.id
        }
    }
    
    var movieTitle: String {
        get {
            return self.movieResultInformation.title
        }
    }
    
    var movieVoteAverage: Float {
        get {
            return self.movieResultInformation.voteAverage
        }
    }
    
    var movieImage: UIImage {
        get {
            return self.movieResultInformation.image
        }
    }
}
