import UIKit

class MovieResultInformation {
    let id: Int
    let title: String
    let voteAverage: Float
    let imageUrl: String
    var image: UIImage
    
    init() {
        self.id = 0
        self.title = ConstantString.defaultMessages.notAvailable
        self.voteAverage = 0.0
        self.imageUrl = ""
        self.image = UIImage(named: ConstantString.imagesNames.movieNotAvailable)!
    }
    
    init(id: Int, title: String, voteAverage: Float, imageUrl: String) {
        self.id = id
        self.title = title
        self.voteAverage = voteAverage
        self.imageUrl = imageUrl
        self.image = UIImage(named: ConstantString.imagesNames.movieNotAvailable)!
    }
}
