import UIKit

class GenreViewModel {
    private let genre: Genre
    
    init(genre: Genre) {
        self.genre = genre
    }
    
    var genreId: Int {
        get {
            return self.genre.id
        }
    }
    
    var genreName: String {
        get {
            return self.genre.name           
        }
    }
}
