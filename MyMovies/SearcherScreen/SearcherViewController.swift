import UIKit

class SearcherViewController: UIViewController,
    UITableViewDelegate,
    UITableViewDataSource,
    SearcherViewModelDelegate {
    
    private let MOVIE_CELL_IDENTIFIER = "movieCellView"
    private let sections = [ConstantString.searcherConstants.sectionSearchByName, ConstantString.searcherConstants.sectionSearchByGenre]
    private let searcherViewModel: SearcherViewModel
    
    @IBOutlet weak var searcherTableView: UITableView!
    
    init(searcherViewModel: SearcherViewModel){
        self.searcherViewModel = searcherViewModel
        super.init(nibName: ConstantString.screensNibNames.searcher, bundle: nil)
        self.searcherViewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = ConstantString.screensTitles.searcher
        self.searcherViewModel.loadAllGenres()
        self.searcherTableView.register(MovieTableViewCell.self, forCellReuseIdentifier: MOVIE_CELL_IDENTIFIER)
        self.searcherTableView.delegate = self
        self.searcherTableView.dataSource = self
    }
    
    // MARK SearcherViewModelDelegate methods
    func genresLoaded() {
        self.searcherTableView.reloadData()
    }
    
    // MARK UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.sections[section]
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2    // To search movies or people
        }
        return self.searcherViewModel.genreViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = searcherTableView.dequeueReusableCell(withIdentifier: MOVIE_CELL_IDENTIFIER, for: indexPath)
        if indexPath.section == 0 {
            cell.textLabel?.text = (indexPath.row == 0)
                ? ConstantString.searcherConstants.sectionByNamePeople
                : ConstantString.searcherConstants.sectionByNameMovies
            return cell
        }
        cell.textLabel?.text = self.searcherViewModel.genreViewModels[indexPath.row].genreName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let searchMovies = (indexPath.row == 0) ? false : true
            let searchByNameResultsViewModel = self.searcherViewModel.getSearchByNameResultsViewModel()
            let vc = SearchByNameResultsViewController(searchMovies: searchMovies, searchByNameResultsViewModel: searchByNameResultsViewModel)
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            let searchByGenreResultsViewModel = self.searcherViewModel.getSearchByGenreResultsViewModel()
            let vc = SearchByGenreResultsViewController(genreId: self.searcherViewModel.genreViewModels[indexPath.row].genreId, searchByGenreResultsViewModel: searchByGenreResultsViewModel)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
