import UIKit

protocol SearcherStorageDelegate: class {
    func genresLoaded(searcherStorage: SearcherStorage)
}

class SearcherStorage: ApiConnector {
    weak var delegate: SearcherStorageDelegate?
    
    var allGenres: [Genre] = []
    
    func loadAllGenres(){
        getJSONFromAPI(url: self.generateUrl())
    }
    
    private func generateUrl() -> URL {
        let url = "\(ConstantString.appSettings.baseUrl)genre/movie/list?api_key=\(ConstantString.appSettings.apiKey)"
        return URL(string: url)!
    }
    
    override func extractInformation(dataInJSON: [String : Any]) {
        guard let genres = dataInJSON["genres"] as? [[String: Any]]
            else {
                print("Error extracting all genres")
                return
        }
        self.extractGenres(genres)
    }
    
    private func extractGenres(_ genresInJson: [[String: Any]]){
        genresInJson.map { (genre) in
            self.extractGenre(genre)
        }
        DispatchQueue.main.async {
            self.delegate?.genresLoaded(searcherStorage: self)
        }
    }
    
    private func extractGenre(_ genre: [String:Any]) {
        guard let id = genre["id"] as? Int,
            let name = genre["name"] as? String
        else {
            print("Error getting genre information")
            return
        }
        self.allGenres.append(Genre(id: id, name: name))
    }
}
