import UIKit

protocol SearcherViewModelDelegate: class {
    func genresLoaded()
}

class SearcherViewModel: SearcherStorageDelegate {
    weak var delegate: SearcherViewModelDelegate?
    
    var genreViewModels: [GenreViewModel] = []
    
    private let searcherStorage: SearcherStorage
    
    init(searcherStorage: SearcherStorage) {
        self.searcherStorage = searcherStorage
        self.searcherStorage.delegate = self
    }
    
    func loadAllGenres(){
        self.searcherStorage.loadAllGenres()
    }
    
    func getSearchByNameResultsViewModel() -> SearchByNameResultsViewModel {
        let searchByNameResultsStorage = SearchByNameResultsStorage()
        return SearchByNameResultsViewModel(searchByNameResultsStorage: searchByNameResultsStorage)
    }
    
    func getSearchByGenreResultsViewModel() -> SearchByGenreResultsViewModel {
        let searchByGenreResultsStorage = SearchByGenreResultsStorage()
        return SearchByGenreResultsViewModel(searchByGenreResultsStorage: searchByGenreResultsStorage)
    }
    
    private func updateViewModels(_ searcherStorage: SearcherStorage){
        self.genreViewModels = searcherStorage.allGenres.map({ (genre) in
            return GenreViewModel(genre: genre)
        })
    }
    
    // MARK SearcherStorageDelegate methods
    func genresLoaded(searcherStorage: SearcherStorage) {
        self.updateViewModels(searcherStorage)
        self.delegate?.genresLoaded()
    }
}
