import UIKit

protocol SearchByGenreResultsStorageDelegate: class {
    func moviesResultsLoaded(searchByGenreResultsStorage: SearchByGenreResultsStorage)
    func loadedImage()
}

class SearchByGenreResultsStorage: ApiConnector {
    weak var delegate: SearchByGenreResultsStorageDelegate?
    
    var allMoviesResult: [MovieResultInformation] = []
    
    func loadMoviesByGenre(genreId: Int, page: Int){
        self.getJSONFromAPI(url: self.generateUrl(genreId: genreId, page: page))
    }
    
    private func generateUrl(genreId: Int, page: Int) -> URL{
        if page == 1 {  // Deleting movies in first call
            allMoviesResult.removeAll()
        }
        let url = "\(ConstantString.appSettings.baseUrl)discover/movie?api_key=\(ConstantString.appSettings.apiKey)&with_genres=\(genreId)&page=\(page)"
        return URL(string: url)!
    }
    
    override func extractInformation(dataInJSON: [String : Any]) {
        guard let allMoviesByGenre = dataInJSON["results"] as? [[String: Any]]
            else {
                print("Error getting all movies by genre")
            return
        }
        allMoviesByGenre.map { (movie) in
            self.allMoviesResult.append(self.extractMovieResult(movie))
        }
        DispatchQueue.main.async {
            self.delegate?.moviesResultsLoaded(searchByGenreResultsStorage: self)
        }
    }
    
    private func extractMovieResult(_ movie: [String: Any]) -> MovieResultInformation{
        guard let id = movie["id"] as? Int,
            let title = movie["title"] as? String,
            let average = movie["vote_average"] as? Float
            else{
                print("Error getting information of movie result")
                return MovieResultInformation()
        }
        guard let imageUrl = movie["poster_path"] as? String
            else{
                return MovieResultInformation(id: id, title: title, voteAverage: average, imageUrl: "")
        }
        let movieResultInformation = MovieResultInformation(id: id, title: title, voteAverage: average, imageUrl: "\(ConstantString.appSettings.baseImageUrl)\(imageUrl)")
        self.downloadImage(object: movieResultInformation,url: movieResultInformation.imageUrl)
        return movieResultInformation
    }
    
    override func saveImageInObject(object: Any, image: UIImage) {
        let movieResultInformation = object as! MovieResultInformation
        movieResultInformation.image = image
        DispatchQueue.main.async {
            self.delegate?.loadedImage()
        }
    }
}
