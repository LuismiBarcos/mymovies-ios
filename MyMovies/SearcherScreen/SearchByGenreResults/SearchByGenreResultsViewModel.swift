import UIKit

protocol SearchByGenreResultsViewModelDelegate: class {
    func moviesLoaded()
    func imageLoaded()
}

class SearchByGenreResultsViewModel: SearchByGenreResultsStorageDelegate {
    weak var delegate: SearchByGenreResultsViewModelDelegate?
    
    var moviesByGenreViewModels: [MovieResultInformationViewModel] = []
    
    private let searchByGenreResultsStorage: SearchByGenreResultsStorage
    
    init(searchByGenreResultsStorage: SearchByGenreResultsStorage) {
        self.searchByGenreResultsStorage = searchByGenreResultsStorage
        self.searchByGenreResultsStorage.delegate = self
    }
    
    func loadMoviesByGenre(genreId: Int, page: Int){
        self.searchByGenreResultsStorage.loadMoviesByGenre(genreId: genreId, page: page)
    }
    
    func getMovieInformationViewModel() -> MovieInformationViewModel {
        let movieInformationStorage = MovieinformationStorage()
        return MovieInformationViewModel(movieInformationStorage: movieInformationStorage)
    }
    
    private func updateMoviesByGenreViewModels(_ searchByGenreResultsStorage: SearchByGenreResultsStorage){
        self.moviesByGenreViewModels = searchByGenreResultsStorage.allMoviesResult.map({ (movie) in
            return MovieResultInformationViewModel(movieResultInformation: movie)
        })
    }
    
    // MARK SearchByGenreResultsStorageDelegate methods
    
    func moviesResultsLoaded(searchByGenreResultsStorage: SearchByGenreResultsStorage) {
        self.updateMoviesByGenreViewModels(searchByGenreResultsStorage)
        self.delegate?.moviesLoaded()
    }
    
    func loadedImage() {
        self.delegate?.imageLoaded()
    }
}
