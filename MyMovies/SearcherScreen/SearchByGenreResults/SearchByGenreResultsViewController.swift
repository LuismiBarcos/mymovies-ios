import UIKit
import UIScrollView_InfiniteScroll

class SearchByGenreResultsViewController: UIViewController,
    UITableViewDelegate,
    UITableViewDataSource,
    SearchByGenreResultsViewModelDelegate
{
    
    private let searchByGenreResultsViewModel: SearchByGenreResultsViewModel
    private let MOVIE_CELL_IDENTIFIER = "movieCellView"
    private let genreId: Int
    
    private var pageOfMoviesToLoad: Int = 1
    private var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet weak var moviesByGenreTableView: UITableView!
    
    init(genreId:Int, searchByGenreResultsViewModel: SearchByGenreResultsViewModel) {
        self.searchByGenreResultsViewModel = searchByGenreResultsViewModel
        self.genreId = genreId
        super.init(nibName: ConstantString.screensNibNames.searchByGenreResults, bundle: nil)
        self.searchByGenreResultsViewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startSpinner(activityIndicator: self.activityIndicator)
        title = ConstantString.screensTitles.searchResults
        self.moviesByGenreTableView.delegate = self
        self.moviesByGenreTableView.dataSource = self
        self.moviesByGenreTableView.register(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: MOVIE_CELL_IDENTIFIER)
        self.searchByGenreResultsViewModel.loadMoviesByGenre(genreId: self.genreId, page: self.pageOfMoviesToLoad)
        self.pageOfMoviesToLoad = self.pageOfMoviesToLoad + 1
        self.moviesByGenreTableView.addInfiniteScroll { (tableView) in
            // update table view
            self.searchByGenreResultsViewModel.loadMoviesByGenre(genreId: self.genreId, page: self.pageOfMoviesToLoad)
            self.pageOfMoviesToLoad = self.pageOfMoviesToLoad + 1
            // finish infinite scroll animation
            tableView.finishInfiniteScroll()
        }
    }
    
    
    // MARK: SearchByGenreResultsViewModelDelegate methods
    
    func moviesLoaded() {
        self.moviesByGenreTableView.reloadData()
        self.stopSpinner(activityIndicator: self.activityIndicator)
    }
    
    func imageLoaded() {
        self.moviesByGenreTableView.reloadData()
    }
    
    // MARK: UITableViewDelegate methods
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchByGenreResultsViewModel.moviesByGenreViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MOVIE_CELL_IDENTIFIER, for: indexPath) as! MovieTableViewCell
        let movieInformation = self.searchByGenreResultsViewModel.moviesByGenreViewModels[indexPath.row]
        cell.principalImage.image = movieInformation.movieImage
        cell.title.text = movieInformation.movieTitle
        cell.subtitle.text = "⭐️ \(movieInformation.movieVoteAverage)/10"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movieInformation = self.searchByGenreResultsViewModel.moviesByGenreViewModels[indexPath.row]
        let movieInformationViewModel = self.searchByGenreResultsViewModel.getMovieInformationViewModel()
        let vc = MovieInformationViewController(movieId: movieInformation.movieId, movieCover: movieInformation.movieImage, movieInformationViewModel: movieInformationViewModel)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
