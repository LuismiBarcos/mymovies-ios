import UIKit

class ApiConnector {
    
    internal func getJSONFromAPI(url: URL){
        let session = URLSession.shared
        let request = URLRequest(url: url)
        let task = session.dataTask(with: request){ (data, response, error) in
            guard let jsonData = data else {
                print("error getting json")
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as! [String:Any]
                self.extractInformation(dataInJSON: json)
            } catch let error as NSError {
                print(error)
            }
        }
        task.resume()
    }
    
    internal func extractInformation(dataInJSON: [String: Any]){
        fatalError("This method must be overridden by the subclass")
    }
    
    internal func downloadImage(object: Any, url: String){
        guard let imageURL = URL(string: url) else{
            return
        }
        let session = URLSession.shared
        let request = URLRequest(url: imageURL)
        let task = session.dataTask(with: request){ (data, response, error) in
            guard let imageData = data else {
                print("error getting image")
                return
            }
            self.saveImageInObject(object: object, image: UIImage(data: imageData)!)
        }
        task.resume()
    }
    
    internal func saveImageInObject(object: Any, image: UIImage){
        fatalError("Class ApiConnector -> This method must be overridden by the subclass")
    }
}
