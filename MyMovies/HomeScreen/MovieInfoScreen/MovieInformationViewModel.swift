import UIKit

protocol MovieInformationViewModelDelegate: class {
    func movieLoaded(_: MovieInformationViewModel)
    func myMoviesZoneInformationLoaded(movieIsFavorite: Bool, movieIsViewed: Bool, movieIsPending: Bool)
}

class MovieInformationViewModel: MovieInformationStorageDelegate {
    
    weak var delegate: MovieInformationViewModelDelegate?
    
    private let movieInformationStorage: MovieinformationStorage
    
    var movieInformationViewModel: MovieInfoViewModel
    
    init(movieInformationStorage: MovieinformationStorage) {
        self.movieInformationStorage = movieInformationStorage
        self.movieInformationViewModel = MovieInfoViewModel(movieInformation:  movieInformationStorage.getTempMovieInformation())
        self.movieInformationStorage.delegate = self
    }
    
    func loadMovieById(movieId: Int){
        movieInformationStorage.loadMovieById(movieId: movieId)
    }
    
    func changeFavoriteValueOfMovie() -> Bool{
        return self.movieInformationStorage.changeFavoriteValueOfMovie(movieInformation: self.movieInformationViewModel.movieInformation)
    }
    
    func changeViewedValueOfMovie() -> Bool {
        return self.movieInformationStorage.changeViewedValueOfMovie(movieInformation: self.movieInformationViewModel.movieInformation)
    }
    
    func changePendingValueOfMovie() -> Bool {
        return self.movieInformationStorage.changePendingValueOfMovie(movieInformation: self.movieInformationViewModel.movieInformation)
    }
    
    func getMovieTeamMembersListViewModel() -> MovieTeamMembersListViewModel {
        let movieTeamMembersListStorage = MovieTeamMembersListStorage()
        return MovieTeamMembersListViewModel(movieTeamMembersListStorage: movieTeamMembersListStorage)
    }
    
    // MARK MovieInformationStorageDelegate methods
    
    func movieInformationLoaded(movieInformation: MovieInformation) {
        self.movieInformationViewModel = MovieInfoViewModel(movieInformation: movieInformation)
        self.delegate?.movieLoaded(self)
    }
    
    func myMoviesZoneInformationLoaded(movieIsFavorite: Bool, movieIsViewed: Bool, movieIsPending: Bool) {
        self.delegate?.myMoviesZoneInformationLoaded(movieIsFavorite: movieIsFavorite,
                                                     movieIsViewed: movieIsViewed,
                                                     movieIsPending: movieIsPending)
    }
}
