import UIKit

class MovieInfoViewModel {
    let movieInformation: MovieInformation
    
    init(movieInformation: MovieInformation) {
        self.movieInformation = movieInformation
    }
    
    var movieId: Int{
        get {
            return movieInformation.id
        }
    }
    
    var movieTitle: String{
        get {
            guard let title = movieInformation.title else {
                return ConstantString.defaultMessages.notAvailable
            }
            return title
        }
    }
    
    var movieGenres: [String]{
        get {
            guard let genres = movieInformation.genres else {
                return [""]
            }
            return genres
        }
    }
    
    var movieOverview: String{
        get {
            guard let overview = movieInformation.overview else {
                return ""
            }
            return overview
        }
    }
    
    var movieAverage: Float{
        get {
            guard let average = movieInformation.average else {
                return 0.0
            }
            return average
        }
    }
    
    var movieVoteCount: Int{
        get {
            guard let voteCount = movieInformation.voteCount else {
                return 0
            }
            return voteCount
        }
    }
    
    var movieReleaseDate: String{
        get {
            guard let releaseDate = movieInformation.releaseDate else {
                return ""
            }
            return releaseDate
        }
    }
    
    var movieCover: UIImage{
        get {
            guard let coverImage = movieInformation.coverImage else {
                return UIImage(named: ConstantString.imagesNames.movieNotAvailable)!
            }
            return coverImage
        }
        set {
            self.movieInformation.coverImage = newValue
        }
    }
}
