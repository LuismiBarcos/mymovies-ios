import UIKit

class MovieInformationViewController: UIViewController,
    UITableViewDataSource,
    UITableViewDelegate,
    MovieInformationViewModelDelegate {

    private let movieId: Int
    private let cover: UIImage
    private let MOVIE_CELL_IDENTIFIER = "movieCellView"
    private let ligthBlue = UIColor(red: 0.0, green: CGFloat(122/255.0), blue: CGFloat(255/255.0), alpha: 100.0)
    
    private let movieInformationViewModel: MovieInformationViewModel
    
    private var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    //Movie information
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieAverage: UILabel!
    @IBOutlet weak var movieVoteCount: UILabel!
    @IBOutlet weak var movieGenres: UILabel!
    @IBOutlet weak var movieReleaseDate: UILabel!
    @IBOutlet weak var movieOverview: UILabel!
    @IBOutlet weak var movieCover: UIImageView!
    
    //Static elements
    @IBOutlet weak var staticElementGenres: UILabel!
    @IBOutlet weak var staticElementReleaseDate: UILabel!
    @IBOutlet weak var staticButtonFavorite: UIButton!
    @IBOutlet weak var staticButtonViewed: UIButton!
    @IBOutlet weak var staticButtonPending: UIButton!
    @IBOutlet weak var staticMovieMembersTableView: UITableView!
    
    init(movieId: Int, movieCover: UIImage, movieInformationViewModel: MovieInformationViewModel){
        self.movieId = movieId
        self.movieInformationViewModel = movieInformationViewModel
        self.cover = movieCover
        super.init(nibName: ConstantString.screensNibNames.movieInformation, bundle: nil)
        self.movieInformationViewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.staticMovieMembersTableView.register(MovieTableViewCell.self, forCellReuseIdentifier: MOVIE_CELL_IDENTIFIER)
        self.staticMovieMembersTableView.delegate = self
        self.staticMovieMembersTableView.dataSource = self
        title = ConstantString.screensTitles.movieInformation
        self.startSpinner(activityIndicator: self.activityIndicator)
        self.showOrHideMovieInformation()
        self.staticMovieMembersTableView.isScrollEnabled = false
        self.movieInformationViewModel.loadMovieById(movieId: movieId)
        self.staticButtonFavorite.layer.cornerRadius = self.staticButtonFavorite.frame.height / 2
        self.staticButtonViewed.layer.cornerRadius = self.staticButtonViewed.frame.height / 2
        self.staticButtonPending.layer.cornerRadius = self.staticButtonPending.frame.height / 2
    }
    
    // Buttons behavior
    
    @IBAction func buttonFavorite(_ sender: UIButton) {
        self.staticButtonFavorite.backgroundColor = self.movieInformationViewModel.changeFavoriteValueOfMovie()
            ? self.ligthBlue
            : .darkGray
    }
    
    @IBAction func buttonViewed(_ sender: UIButton) {
        self.staticButtonViewed.backgroundColor = self.movieInformationViewModel.changeViewedValueOfMovie()
            ? self.ligthBlue
            : .darkGray
    }
    @IBAction func buttonPending(_ sender: UIButton) {
        self.staticButtonPending.backgroundColor = self.movieInformationViewModel.changePendingValueOfMovie()
            ? self.ligthBlue
            : .darkGray
    }
    
    // Handle information
    
    private func showMovieInformation(){
        self.movieTitle.text = self.movieInformationViewModel.movieInformationViewModel.movieTitle
        self.movieCover.image = self.movieInformationViewModel.movieInformationViewModel.movieCover
        self.movieAverage.text = "\(self.movieInformationViewModel.movieInformationViewModel.movieAverage)/10"
        self.movieVoteCount.text = "Over \(self.movieInformationViewModel.movieInformationViewModel.movieVoteCount) votes"
        self.movieGenres.text = self.convertGenresToString(self.movieInformationViewModel.movieInformationViewModel.movieGenres)
        self.movieReleaseDate.text = self.movieInformationViewModel.movieInformationViewModel.movieReleaseDate
        self.movieOverview.text = self.movieInformationViewModel.movieInformationViewModel.movieOverview
    }
    
    private func convertGenresToString(_ genres: [String]) -> String {
        return genres.flatMap({$0}).joined(separator: ", ")
    }
    
    func showOrHideMovieInformation(){
        self.movieTitle.isHidden = !self.movieTitle.isHidden
        self.movieCover.isHidden = !self.movieCover.isHidden
        self.movieAverage.isHidden = !self.movieAverage.isHidden
        self.movieVoteCount.isHidden = !self.movieVoteCount.isHidden
        self.movieGenres.isHidden = !self.movieGenres.isHidden
        self.movieReleaseDate.isHidden = !self.movieReleaseDate.isHidden
        self.movieOverview.isHidden = !self.movieOverview.isHidden
        self.staticElementGenres.isHidden = !self.staticElementGenres.isHidden
        self.staticElementReleaseDate.isHidden = !self.staticElementReleaseDate.isHidden
        self.staticButtonFavorite.isHidden = !self.staticButtonFavorite.isHidden
        self.staticButtonViewed.isHidden = !self.staticButtonViewed.isHidden
        self.staticButtonPending.isHidden = !self.staticButtonPending.isHidden
        self.staticMovieMembersTableView.isHidden = !self.staticMovieMembersTableView.isHidden
    }
    
    // MARK MovieInformationViewModelDelegate methods
    
    func movieLoaded(_: MovieInformationViewModel) {
        self.movieInformationViewModel.movieInformationViewModel.movieCover = self.cover
        self.showMovieInformation()
        self.stopSpinner(activityIndicator: self.activityIndicator)
        self.showOrHideMovieInformation()
    }
    
    func myMoviesZoneInformationLoaded(movieIsFavorite: Bool, movieIsViewed: Bool, movieIsPending: Bool) {
        self.staticButtonFavorite.backgroundColor = movieIsFavorite
            ? self.ligthBlue
            : .darkGray
    
        self.staticButtonViewed.backgroundColor = movieIsViewed
            ? self.ligthBlue
            : .darkGray
    

        self.staticButtonPending.backgroundColor = movieIsPending
            ? self.ligthBlue
            : .darkGray
    }
    
    // MARK UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MOVIE_CELL_IDENTIFIER, for: indexPath)
        cell.textLabel?.text = indexPath.row == 0
            ? ConstantString.movieInformationConstants.viewCast
            : ConstantString.movieInformationConstants.viewTeam
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let movieTeamMembersListViewModel = self.movieInformationViewModel.getMovieTeamMembersListViewModel()
        let vc = MovieTeamMembersListViewController(movieId: self.movieId, showCast: self.viewCastPressed(indexPath), movieTeamMembersListViewModel: movieTeamMembersListViewModel)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private func viewCastPressed(_ indexPath: IndexPath) -> Bool {
        return indexPath.row == 0
    }
}
