import Foundation
import SQLite

protocol MovieInformationStorageDelegate: class {
    func movieInformationLoaded(movieInformation: MovieInformation)
    func myMoviesZoneInformationLoaded(movieIsFavorite: Bool, movieIsViewed: Bool, movieIsPending: Bool)
}

class MovieinformationStorage: ApiConnector {
    
    weak var delegate: MovieInformationStorageDelegate?

    private var fakeMovie: MovieInformation?
    
    private var db: Connection?
    private var myMoviesZoneInformation: MyMoviesZoneInformation?
    
    override init() {
        super.init()
        self.makeDBConnection()
    }
    
    private func makeDBConnection() {
        let directory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        do {
            let path = directory[0] + "/db.sqlite"
            db = try Connection(path)
            print("BD abierta en \(path)\n")
            } catch {
            print("Error abriendo BD\n")
        }
        self.fakeMovie = MovieInformation(conn: db!, id: 0, title: "Movie not available", genres: [""], voteCount: 0, overview: "", average: 0.0, releaseDate: "")
        fakeMovie?.createTable()
    }
    
    func loadMovieById(movieId: Int){
        self.myMoviesZoneInformation = MyMoviesZoneInformation(movieId: movieId, conn: db!)
        self.myMoviesZoneInformation?.createTable()
        self.delegate?.myMoviesZoneInformationLoaded(movieIsFavorite: self.myMoviesZoneInformation!.favorite,
                                                     movieIsViewed: self.myMoviesZoneInformation!.viewed,
                                                     movieIsPending: self.myMoviesZoneInformation!.pending)
        if self.movieIsInDB(){
            self.loadMovieFromDB(movieId: movieId)
        } else {
            getJSONFromAPI(url: generateUrl(movieId: movieId))
        }
    }
    
    private func movieIsInDB() -> Bool {
        return self.myMoviesZoneInformation!.favorite != false || self.myMoviesZoneInformation!.viewed != false || self.myMoviesZoneInformation!.pending != false
    }
    
    private func loadMovieFromDB(movieId: Int) {
        self.delegate?.movieInformationLoaded(movieInformation: MovieInformation(conn: db!, movieId: movieId))
    }
    
    func changeFavoriteValueOfMovie(movieInformation: MovieInformation) -> Bool{
        self.myMoviesZoneInformation?.favorite = !(self.myMoviesZoneInformation?.favorite)!
        self.myMoviesZoneInformation?.save()
        self.saveOrDeleteMovieInformation(movieInformation)
        return (self.myMoviesZoneInformation?.favorite)!
    }
    
    func changeViewedValueOfMovie(movieInformation: MovieInformation) -> Bool{
        self.myMoviesZoneInformation?.viewed = !(self.myMoviesZoneInformation?.viewed)!
        self.myMoviesZoneInformation?.save()
        self.saveOrDeleteMovieInformation(movieInformation)
        return (self.myMoviesZoneInformation?.viewed)!
    }
    
    func changePendingValueOfMovie(movieInformation: MovieInformation) -> Bool{
        self.myMoviesZoneInformation?.pending = !(self.myMoviesZoneInformation?.pending)!
        self.myMoviesZoneInformation?.save()
        self.saveOrDeleteMovieInformation(movieInformation)
        return (self.myMoviesZoneInformation?.pending)!
    }
    
    private func saveOrDeleteMovieInformation(_ movieInformation: MovieInformation) {
        if (self.myMoviesZoneInformation?.favorite)! == false && (self.myMoviesZoneInformation?.viewed)! == false && (self.myMoviesZoneInformation?.pending)! == false {
            movieInformation.delete()
        } else {
            movieInformation.save()
        }
    }
    
    func getTempMovieInformation() -> MovieInformation{
        return fakeMovie!
    }
    
    private func generateUrl(movieId: Int) -> URL {
        return URL(string: "\(ConstantString.appSettings.baseMoviesUrl)\(movieId)?api_key=\(ConstantString.appSettings.apiKey)")!
    }
    
    override func extractInformation(dataInJSON json: [String : Any]) {
        guard let id = json["id"] as? Int,
            let title = json["original_title"] as? String,
            let genres = json["genres"] as?  [[String: Any]],
            let overview = json["overview"] as? String,
            let average = json["vote_average"] as? Float,
            let voteCount = json["vote_count"] as? Int,
            let releaseDate = json["release_date"] as? String
        else {
            print("Error extracting movie information")
            return
        }
        let movieInformation = MovieInformation(conn: db!, id: id, title: title, genres: extractGenres(genres),voteCount: voteCount,
                                                overview: overview, average: average, releaseDate: releaseDate)
        DispatchQueue.main.async {
            self.delegate?.movieInformationLoaded(movieInformation: movieInformation)
        }
    }
    
    private func extractGenres(_ genresInJSON: [[String: Any]]) -> [String]{
        return genresInJSON.map({ (genre) -> String in
            genre["name"] as! String
        })
    }
}
