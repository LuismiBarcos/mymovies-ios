import UIKit

protocol MoviesStorageDelegate: class {
    func dataLoaded(moviesStorage: MoviesStorage, moviesData: [MovieBasicInfo])
    func coversLoaded(moviesStorage: MoviesStorage)
}

class MoviesStorage: ApiConnector {
    weak var delegate: MoviesStorageDelegate?
    internal(set) var movies: [MovieBasicInfo] = []
    
    func loadData(page: Int){
        getJSONFromAPI(url: generateUrl(page: page))
    }
    
    private func generateUrl(page: Int) -> URL{
        return URL(string: "\(ConstantString.appSettings.baseMoviesUrl)upcoming?api_key=\(ConstantString.appSettings.apiKey)&page=\(page)")!
    }
    
    internal func cleanMovies(){
        self.movies.removeAll()
    }
    
    override internal func extractInformation(dataInJSON: [String:Any]){
        let allMovies = dataInJSON["results"] as? [[String: Any]] ?? []
        self.extractAndSaveMoviesFromJSON(allMovies: allMovies)
        DispatchQueue.main.async {
            self.delegate?.dataLoaded(moviesStorage: self, moviesData: self.movies)
        }
    }
    
    private func extractAndSaveMoviesFromJSON(allMovies: [[String:Any]]){
        allMovies.map({ (movie) in
            self.movies.append(extractMovieFromJSON(movie: movie))
        })
    }
    
    private func extractMovieFromJSON(movie: [String: Any]) -> MovieBasicInfo {
        guard let id = movie["id"] as? Int,
            let title = movie["original_title"] as? String,
            let voteAverage = movie["vote_average"] as? Float
            else{
                print("error")
                return MovieBasicInfo()
        }
        guard let imageURL = movie["poster_path"] as? String
            else {
                let movieBasicInfo = MovieBasicInfo(id: id, title: title, voteAverage: voteAverage, imageURL: "")
                self.downloadImage(object: movieBasicInfo, url: movieBasicInfo.url)
                return movieBasicInfo
        }
        let movieBasicInfo = MovieBasicInfo(id: id, title: title, voteAverage: voteAverage, imageURL: "\(ConstantString.appSettings.baseImageUrl)\(imageURL)")
        self.downloadImage(object: movieBasicInfo, url: movieBasicInfo.url)
        return movieBasicInfo
    }
    
    override func saveImageInObject(object: Any, image: UIImage) {
        let movieBasicInfo = object as! MovieBasicInfo
        movieBasicInfo.image = image
        DispatchQueue.main.async {
            self.delegate?.coversLoaded(moviesStorage: self)
        }
    }
}
