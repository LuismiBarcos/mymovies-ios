import UIKit

class MovieBasicInfoViewModel {
    private let movieInfo: MovieBasicInfo
    
    init(movieInfo: MovieBasicInfo) {
        self.movieInfo = movieInfo
    }
    
    var id: Int {
        get {
            return movieInfo.id
        }
    }
    
    var title: String {
        get {
            return movieInfo.title
        }
    }
    
    var voteAverage: Float {
        get {
            return movieInfo.voteAverage
        }
    }
    
    var image: UIImage {
        get {
            return movieInfo.image
        }
    }
}
