import UIKit

class MovieMemberViewModel {
    private let movieMember : MovieMember
    
    init(movieMember: MovieMember) {
        self.movieMember = movieMember
    }
    
    var memberId: Int{
        get {
            return self.movieMember.id
        }
    }
    
    var memberName: String{
        get {
            return self.movieMember.name
        }
    }
    
    var memberPerformance: String{
        get {
            return self.movieMember.performance
        }
    }
    
    var memberImage: UIImage{
        get {
            return self.movieMember.image
        }
    }
    
    var id: Int{
        get {
            return self.movieMember.id
        }
    }
}
