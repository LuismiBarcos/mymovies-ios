import UIKit

class MovieTeamMembersListViewController:
    UIViewController,
    UITableViewDataSource,
    UITableViewDelegate,
    MovieTeamMembersListViewModelDelegate {

    private let MOVIE_CELL_IDENTIFIER = "movieCellView"
    private let movieId: Int
    private let showCast: Bool
    private let movieTeamMembersListViewModel: MovieTeamMembersListViewModel
    private var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet weak var movieTeamMembersTableView: UITableView!
    
    init(movieId: Int, showCast: Bool, movieTeamMembersListViewModel: MovieTeamMembersListViewModel){
        self.movieTeamMembersListViewModel = movieTeamMembersListViewModel
        self.movieId = movieId
        self.showCast = showCast
        super.init(nibName: ConstantString.screensNibNames.movieTeamMembersList, bundle: nil)
        self.movieTeamMembersListViewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startSpinner(activityIndicator: self.activityIndicator)
        title = ConstantString.screensTitles.personalInformation
        (self.showCast)
            ? self.movieTeamMembersListViewModel.loadMovieCast(movieId: movieId)
            : self.movieTeamMembersListViewModel.loadMovieTeam(movieId: movieId)
        self.movieTeamMembersTableView.register(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: MOVIE_CELL_IDENTIFIER)
        self.movieTeamMembersTableView.delegate = self
        self.movieTeamMembersTableView.dataSource = self
    }
    
    // MARK MovieTeamMembersListViewModelDelegate methods
    
    func loadedMovieMembers() {
        self.movieTeamMembersTableView.reloadData()
        self.stopSpinner(activityIndicator: self.activityIndicator)
    }
    
    // MARK UITableViewDataSource methods
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieTeamMembersListViewModel.movieMemberViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MOVIE_CELL_IDENTIFIER, for: indexPath) as! MovieTableViewCell
        let movieMember = movieTeamMembersListViewModel.movieMemberViewModels[indexPath.row]
        cell.principalImage.image = movieMember.memberImage
        cell.title.text = movieMember.memberName
        cell.subtitle.text = movieMember.memberPerformance
        cell.subtitle.textColor = UIColor.gray
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let member = self.movieTeamMembersListViewModel.movieMemberViewModels[indexPath.row]
        let movieMemberInformationViewModel = self.movieTeamMembersListViewModel.getMovieMemberInformationViewModel()
        let vc = MovieMemberInformationViewController(memberId: member.id, memberImage: member.memberImage, movieMemberInformationViewModel: movieMemberInformationViewModel)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
