import UIKit

protocol MovieTeamMembersListViewModelDelegate: class{
    func loadedMovieMembers()
}

class MovieTeamMembersListViewModel: MovieTeamMembersListStorageDelegate {
    weak var delegate: MovieTeamMembersListViewModelDelegate?
    
    private(set) var movieMemberViewModels: [MovieMemberViewModel] = []
    
    private let movieTeamMembersListStorage: MovieTeamMembersListStorage
    
    init(movieTeamMembersListStorage: MovieTeamMembersListStorage) {
        self.movieTeamMembersListStorage = movieTeamMembersListStorage
        self.movieTeamMembersListStorage.delegate = self
    }
    
    func loadMovieCast(movieId: Int) {
        self.movieTeamMembersListStorage.loadMovieCast(movieId: movieId)
    }
    
    func loadMovieTeam(movieId: Int) {
        self.movieTeamMembersListStorage.loadMovieTeam(movieId: movieId)
    }
    
    func getMovieMemberInformationViewModel() -> MovieMemberInformationViewModel {
        let movieMemberInformationStorage = MovieMemberInformationStorage()
        return MovieMemberInformationViewModel(movieMemberInformationStorage: movieMemberInformationStorage)
    }
    
    private func udpateMovieMemberViewModels(membersData: [MovieMember]) {
        movieMemberViewModels = membersData.map({ (member: MovieMember) -> MovieMemberViewModel in
            return MovieMemberViewModel(movieMember: member)
        })
    }
    
    // MARK MovieTeamMembersListStorageDelegate methods
    func loadedCast(cast: [MovieMember]) {
        self.udpateMovieMemberViewModels(membersData: cast)
        self.delegate?.loadedMovieMembers()
    }
    
    func loadedTeam(team: [MovieMember]) {
        self.udpateMovieMemberViewModels(membersData: team)
        self.delegate?.loadedMovieMembers()
    }
    
    func loadedImage() {
        self.delegate?.loadedMovieMembers()
    }
}
