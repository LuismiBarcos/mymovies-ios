import UIKit

class MovieMember {
    let id: Int
    let name: String
    let performance: String
    var image: UIImage
    let imageUrl: String
    
    init() {
        self.id = 0
        self.name = ConstantString.defaultMessages.notAvailable
        self.performance = ""
        self.image = UIImage(named: ConstantString.imagesNames.imageNotAvailable)!
        self.imageUrl = ""
    }
    
    init(id: Int, name: String, performance: String, imageUrl: String) {
        self.id = id
        self.name = name
        self.performance = performance
        self.image = UIImage(named: ConstantString.imagesNames.imageNotAvailable)!
        self.imageUrl = imageUrl
    }
}
