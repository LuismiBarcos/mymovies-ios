import UIKit

protocol MovieTeamMembersListStorageDelegate:class {
    func loadedCast(cast: [MovieMember])
    func loadedTeam(team: [MovieMember])
    func loadedImage()
}

class MovieTeamMembersListStorage: ApiConnector {
    
    private var loadCast: Bool?
    
    weak var delegate: MovieTeamMembersListStorageDelegate?
    
    func loadMovieCast(movieId: Int) {
        self.loadCast = true
        getJSONFromAPI(url: generateUrl(movieId))
    }
    
    func loadMovieTeam(movieId: Int){
        self.loadCast = false
        getJSONFromAPI(url: generateUrl(movieId))
    }
    
    private func generateUrl(_ movieId: Int) -> URL{
        return URL(string: "\(ConstantString.appSettings.baseMoviesUrl)\(movieId)/credits?api_key=\(ConstantString.appSettings.apiKey)")!
    }
    
    override func extractInformation(dataInJSON: [String : Any]) {
        (self.loadCast)!
            ? self.extractCast(dataInJSON)
            : self.extractTeam(dataInJSON)
    }
    
    private func extractCast(_ json: [String: Any]) {
        let castInJSON = json["cast"] as? [[String: Any]] ?? []
        let cast = castInJSON.map { (actor) in
            return self.extractActor(actor)
        }
        DispatchQueue.main.async {
            self.delegate?.loadedCast(cast: cast)
        }
    }
    
    private func extractTeam(_ json: [String: Any]) {
        let teamInJSON = json["crew"] as? [[String: Any]] ?? []
        let team = teamInJSON.map { (member) in
            return self.extractTeamMember(member)
        }
        DispatchQueue.main.async {
            self.delegate?.loadedTeam(team: team)
        }
    }
    
    private func extractActor(_ actor: [String: Any]) -> MovieMember{
        guard let id = actor["id"] as? Int,
            let name = actor["name"] as? String,
            let performance = actor["character"] as? String
        else {
            print("Error extrayendo informacion del actor")
            return MovieMember()
        }
        guard let imageUrl = actor["profile_path"] as? String
            else {
                let movieMember = MovieMember(id: id, name: name, performance: performance, imageUrl: "not available")
                return movieMember
        }
        let movieMember = MovieMember(id: id, name: name, performance: performance, imageUrl: "\(ConstantString.appSettings.baseImageUrl)\(imageUrl)")
        self.downloadImage(object: movieMember, url: movieMember.imageUrl)
        return movieMember
    }
    
    private func extractTeamMember(_ member: [String:Any]) -> MovieMember{
        guard let id = member["id"] as? Int,
            let name = member["name"] as? String,
            let performance = member["job"] as? String
            else {
                print("Error extrayendo informacion del actor")
                return MovieMember()
        }
        guard let imageUrl = member["profile_path"] as? String
            else {
                let movieMember = MovieMember(id: id, name: name, performance: performance, imageUrl: "not available")
                return movieMember
        }
        let movieMember = MovieMember(id: id, name: name, performance: performance, imageUrl: "\(ConstantString.appSettings.baseImageUrl)\(imageUrl)")
        self.downloadImage(object: movieMember, url: movieMember.imageUrl)
        return movieMember
    }
    
    override func saveImageInObject(object: Any, image: UIImage) {
        let movieMember = object as! MovieMember
        movieMember.image = image
        DispatchQueue.main.async {
            self.delegate?.loadedImage()
        }
    }
}
