import Foundation
import UIKit

protocol HomeViewModelDelegate: class {
    func homeViewModelDidLoadMovies(_: HomeViewModel)
}

class HomeViewModel: MoviesStorageDelegate {
    private let upcomingMoviesStorage: UpcomingMoviesStorage
    private let topRatedMoviesStorage: TopRatedMoviesStorage
    private let popularMoviesStorage: PopularMoviesStorage
    
    weak var delegate: HomeViewModelDelegate?
    private(set) var movieInfoViewModels: [MovieBasicInfoViewModel] = []
    
    init(upcomingMoviesStorage: UpcomingMoviesStorage,
         topRatedMoviesStorage: TopRatedMoviesStorage,
         popularMoviesStorage: PopularMoviesStorage) {
        self.upcomingMoviesStorage = upcomingMoviesStorage
        self.topRatedMoviesStorage = topRatedMoviesStorage
        self.popularMoviesStorage = popularMoviesStorage
        // Delegates
        self.upcomingMoviesStorage.delegate = self
        self.topRatedMoviesStorage.delegate = self
        self.popularMoviesStorage.delegate = self
    }
    
    func loadUpcomingMovies(page: Int){
        upcomingMoviesStorage.loadData(page: page)
    }
    
    func loadTopRatedMovies(page: Int){
        topRatedMoviesStorage.loadData(page: page)
    }
    
    func loadPopularMovies(page: Int) {
        popularMoviesStorage.loadData(page: page)
    }
    
    func getMovieInformationViewModel() -> MovieInformationViewModel {
        let movieInformationStorage = MovieinformationStorage()
        return MovieInformationViewModel(movieInformationStorage: movieInformationStorage)
    }
    
    private func udpateMovieInfoViewModels(moviesStorage: MoviesStorage) {
        movieInfoViewModels = moviesStorage.movies.map({ (movieInfo: MovieBasicInfo) -> MovieBasicInfoViewModel in
            return MovieBasicInfoViewModel(movieInfo: movieInfo)
        })
    }
    
    // MARK MoviesStorageDelegate methods
    
    func dataLoaded(moviesStorage: MoviesStorage, moviesData: [MovieBasicInfo]) {
        udpateMovieInfoViewModels(moviesStorage: moviesStorage)
        delegate?.homeViewModelDidLoadMovies(self)
    }
    
    func coversLoaded(moviesStorage: MoviesStorage) {
        udpateMovieInfoViewModels(moviesStorage: moviesStorage)
        delegate?.homeViewModelDidLoadMovies(self)
    }
}
