import UIKit

class UpcomingMoviesStorage: MoviesStorage {
    override func loadData(page: Int) {
        getJSONFromAPI(url: generateUrl(page: page))
    }
    
    private func generateUrl(page: Int) -> URL {
        if page == 1 {  // Deleting movies in first call
            self.cleanMovies()
        }
        return URL(string: "\(ConstantString.appSettings.baseMoviesUrl)upcoming?api_key=\(ConstantString.appSettings.apiKey)&page=\(page)")!
    }
}
