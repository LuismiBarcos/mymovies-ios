import UIKit

class MovieMemberInformation {
    let id: Int
    let name: String
    let biography: String
    let birthday: String
    let deathday: String
    let image: UIImage
    
    init() {
        self.id = 0
        self.name = ConstantString.defaultMessages.notAvailable
        self.biography = ""
        self.birthday = ""
        self.deathday = ""
        self.image = UIImage(named: ConstantString.imagesNames.imageNotAvailable)!
    }
    
    init(id: Int, name: String, biography: String, birthday: String, deathday: String, image: UIImage){
        self.id = id
        self.name = name
        self.biography = biography
        self.birthday = birthday
        self.deathday = deathday
        self.image = image
    }
}
