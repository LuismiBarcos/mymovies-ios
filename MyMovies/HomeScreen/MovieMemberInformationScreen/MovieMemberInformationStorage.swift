import UIKit

protocol MovieMemberInformationStorageDelegate: class {
    func memberInformationLoaded(movieMember: MovieMemberInformation)
    //func memberImageLoaded()
}

class MovieMemberInformationStorage: ApiConnector {
    weak var delegate: MovieMemberInformationStorageDelegate?
    
    func loadMovieMember(memberId: Int) {
        // Result going to extractInformation
        self.getJSONFromAPI(url: generateUrl(memberId))
    }
    
    private func generateUrl(_ memberId: Int) -> URL{
        let url = "\(ConstantString.appSettings.baseUrl)person/\(memberId)?api_key=\(ConstantString.appSettings.apiKey)"
        return URL(string: url)!
    }
    
    override func extractInformation(dataInJSON: [String : Any]) {
        guard let id = dataInJSON["id"] as? Int,
            let name = dataInJSON["name"] as? String,
            let biography = dataInJSON["biography"] as? String,
            let birthday = dataInJSON["birthday"] as? String
            else {
                print("Error extracting information of movie member")
                return
        }
        
        guard let deathday = dataInJSON["deathday"] as? String
            else {
                let movieMember = MovieMemberInformation(id: id, name: name, biography: biography, birthday: birthday, deathday: "", image: UIImage(named: ConstantString.imagesNames.imageNotAvailable)!)
                notifyMainThreadImageLoaded(object: movieMember)
                return
        }
        
        let movieMember = MovieMemberInformation(id: id, name: name, biography: biography, birthday: birthday, deathday: deathday, image: UIImage(named: ConstantString.imagesNames.imageNotAvailable)!)
        notifyMainThreadImageLoaded(object: movieMember)
    }
    
    private func notifyMainThreadImageLoaded(object: Any) {
        let movieMember = object as! MovieMemberInformation
        DispatchQueue.main.async {
            self.delegate?.memberInformationLoaded(movieMember: movieMember)
        }
    }
}
