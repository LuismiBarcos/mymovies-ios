import UIKit

class MovieMemberInformationViewController: UIViewController, MovieMemberInformationViewModelDelegate {

    private let movieMemberInformationViewModel: MovieMemberInformationViewModel
    private let memberId: Int
    private let memberImage: UIImage
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    //MovieMemberInformation
    @IBOutlet weak var movieMemberName: UILabel!
    @IBOutlet weak var movieMemberBirthday: UILabel!
    @IBOutlet weak var movieMemberDeathday: UILabel!
    @IBOutlet weak var movieMemberBiography: UILabel!
    @IBOutlet weak var movieMemberImage: UIImageView!
    
    //Static elements
    @IBOutlet weak var buttonShowActorMovies: UIButton!
    @IBOutlet weak var buttonShowCrewMemberMovies: UIButton!
    
    
    // Buttons behavior
    @IBAction func showMoviesAsActor(_ sender: UIButton) {
        self.showMoviesOfMovieMember(showMoviesAsCastMember: true)
    }
    
    @IBAction func showMoviesAsCrewMember(_ sender: UIButton) {
        self.showMoviesOfMovieMember(showMoviesAsCastMember: false)
    }
    
    
    init(memberId: Int, memberImage: UIImage, movieMemberInformationViewModel: MovieMemberInformationViewModel){
        self.movieMemberInformationViewModel = movieMemberInformationViewModel
        self.memberId = memberId
        self.memberImage = memberImage
        super.init(nibName: ConstantString.screensNibNames.movieMemberInformation, bundle: nil)
        self.movieMemberInformationViewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = ConstantString.screensTitles.personalInformation
        self.movieMemberInformationViewModel.loadMovieMemberInformation(memberId: self.memberId)
        self.startSpinner(activityIndicator: self.activityIndicator)
        self.showOrHideMemberInformation()
        self.buttonShowActorMovies.layer.cornerRadius = self.buttonShowActorMovies.frame.height / 2
        self.buttonShowCrewMemberMovies.layer.cornerRadius = self.buttonShowCrewMemberMovies.frame.height / 2
    }
    
    private func showOrHideMemberInformation(){
        self.movieMemberName.isHidden = !self.movieMemberName.isHidden
        self.movieMemberBirthday.isHidden = !self.movieMemberBirthday.isHidden
        self.movieMemberDeathday.isHidden = !self.movieMemberDeathday.isHidden
        self.movieMemberBiography.isHidden = !self.movieMemberBiography.isHidden
        self.movieMemberImage.isHidden = !self.movieMemberImage.isHidden
        self.buttonShowActorMovies.isHidden = !self.buttonShowActorMovies.isHidden
        self.buttonShowCrewMemberMovies.isHidden = !self.buttonShowCrewMemberMovies.isHidden
    }
    
    private func putMemberInformation(){
        self.movieMemberName.text = self.movieMemberInformationViewModel.memberName
        self.movieMemberBirthday.text = self.movieMemberInformationViewModel.memberBirthday
        self.movieMemberDeathday.text = self.movieMemberInformationViewModel.memberDeathday
        self.movieMemberBiography.text = self.movieMemberInformationViewModel.memberBiography
        self.movieMemberImage.image = self.memberImage
    }
    
    private func showMoviesOfMovieMember(showMoviesAsCastMember: Bool){
        let movieMemberMoviesListViewModel = self.movieMemberInformationViewModel.getMovieMemberMoviesListViewModel()
        let vc = MovieMemberMoviesListViewController(memberId: self.memberId, showMoviesAsCastMember: showMoviesAsCastMember, movieMemberMoviesListViewModel: movieMemberMoviesListViewModel)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK MovieMemberInformationViewModelDelegate methods
    func movieMemberInformationLoaded() {
        self.putMemberInformation()
        self.stopSpinner(activityIndicator: self.activityIndicator)
        self.showOrHideMemberInformation()
    }
}
