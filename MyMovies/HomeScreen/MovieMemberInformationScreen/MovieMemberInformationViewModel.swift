import UIKit

protocol MovieMemberInformationViewModelDelegate: class {
    func movieMemberInformationLoaded()
}

class MovieMemberInformationViewModel: MovieMemberInformationStorageDelegate {
    
    weak var delegate: MovieMemberInformationViewModelDelegate?
    
    private let movieMemberInformationStorage: MovieMemberInformationStorage
    private var movieMemberInformation: MovieMemberInformation
    
    init(movieMemberInformationStorage: MovieMemberInformationStorage) {
        self.movieMemberInformationStorage = movieMemberInformationStorage
        self.movieMemberInformation = MovieMemberInformation()
        self.movieMemberInformationStorage.delegate = self
    }
    
    func loadMovieMemberInformation(memberId: Int){
        self.movieMemberInformationStorage.loadMovieMember(memberId: memberId)
    }
    
    func getMovieMemberMoviesListViewModel() -> MovieMemberMoviesListViewModel {
        let movieMemberMoviesListStorage = MovieMemberMoviesListStorage()
        return MovieMemberMoviesListViewModel(movieMemberMoviesListStorage: movieMemberMoviesListStorage)
    }
    
    // MARK MovieMemberInformationStorageDelegate methods
    func memberInformationLoaded(movieMember: MovieMemberInformation) {
        self.movieMemberInformation = movieMember
        self.delegate?.movieMemberInformationLoaded()
    }
    
    // MovieMemberInformation properties
    
    var memberId: Int {
        get {
            return self.movieMemberInformation.id
        }
    }
    
    var memberName: String {
        get {
            return self.movieMemberInformation.name
        }
    }
    
    var memberBiography: String {
        get {
            return self.movieMemberInformation.biography
        }
    }
    
    var memberBirthday: String {
        get {
            return self.movieMemberInformation.birthday
        }
    }
    
    var memberDeathday: String {
        get {
            return self.movieMemberInformation.deathday
        }
    }
    
    var memberImage: UIImage {
        get {
            return self.movieMemberInformation.image
        }
    }
}
