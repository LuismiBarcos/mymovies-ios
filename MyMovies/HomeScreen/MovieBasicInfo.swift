import UIKit

class MovieBasicInfo {
    let id: Int
    let title: String
    let voteAverage: Float
    var image: UIImage
    let url: String
    
    init(){
        self.id = 0
        self.title = ConstantString.defaultMessages.notAvailable
        self.voteAverage = 0.0
        self.image = UIImage(named: ConstantString.imagesNames.movieNotAvailable)!
        self.url = ""
    }
    
    init(id: Int, title: String, voteAverage: Float, imageURL: String) {
        self.id = id
        self.title = title
        self.voteAverage = voteAverage
        self.image = UIImage(named: ConstantString.imagesNames.movieNotAvailable)!
        self.url = imageURL
    }
    
    
}
