import UIKit

class MovieMemberMovieInformation {
    let id: Int
    let title: String
    let performance: String
    let voteAverage: Float
    let coverUrl: String
    var cover = UIImage(named: ConstantString.imagesNames.movieNotAvailable)!
    
    init() {
        self.id = 0
        self.title = ConstantString.defaultMessages.notAvailable
        self.performance = ""
        self.voteAverage = 0.0
        self.coverUrl = ""
    }
    
    init(id: Int, title: String, performance: String, voteAverage: Float, coverUrl: String) {
        self.id = id
        self.title = title
        self.performance = performance
        self.voteAverage = voteAverage
        self.coverUrl = coverUrl
    }
}
