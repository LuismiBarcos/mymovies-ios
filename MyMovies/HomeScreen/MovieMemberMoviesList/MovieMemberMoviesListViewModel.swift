import UIKit

protocol MovieMemberMoviesListViewModelDelegate: class {
    func moviesLoaded()
}

class MovieMemberMoviesListViewModel: MovieMemberMoviesListStorageDelegate {
    weak var delegate: MovieMemberMoviesListViewModelDelegate?
    
    private let movieMemberMoviesListStorage: MovieMemberMoviesListStorage
    
    var movieMemberMovieInformationViewModels: [MovieMemberMovieInformationViewModel] = []
    
    init(movieMemberMoviesListStorage: MovieMemberMoviesListStorage) {
        self.movieMemberMoviesListStorage = movieMemberMoviesListStorage
        self.movieMemberMoviesListStorage.delegate = self
    }
    
    func loadMoviesOfMovieMember(memberId: Int, showMoviesAsCastMember: Bool){
        (showMoviesAsCastMember)
        ? self.movieMemberMoviesListStorage.loadMoviesOfMovieMemberAsMemberOfCast(memberId: memberId)
        :self.movieMemberMoviesListStorage.loadMoviesOfMovieMemberAsMemberOfCrew(memberId: memberId)
    }
    
    func getMovieInformationViewModel() -> MovieInformationViewModel {
        let movieInformationStorage = MovieinformationStorage()
        return MovieInformationViewModel(movieInformationStorage: movieInformationStorage)
    }
    
    private func udpateMovieInfoViewModels(movieMemberMoviesListStorage: MovieMemberMoviesListStorage) {
        movieMemberMovieInformationViewModels = movieMemberMoviesListStorage.allMovies.map({ (movieInfo: MovieMemberMovieInformation) -> MovieMemberMovieInformationViewModel in
            return MovieMemberMovieInformationViewModel(movieMemberMovieInformation: movieInfo)
        })
    }
    
    // MARK MovieMemberMoviesListStorageDelegate methods
    func moviesLoaded(movieMemberMoviesListStorage: MovieMemberMoviesListStorage) {
        self.udpateMovieInfoViewModels(movieMemberMoviesListStorage: movieMemberMoviesListStorage)
        self.delegate?.moviesLoaded()
    }
    
    func coverLoaded() {
        self.delegate?.moviesLoaded()
    }
}
