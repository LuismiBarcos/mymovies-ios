import UIKit

class MovieMemberMoviesListViewController:
    UIViewController,
    UITableViewDataSource,
    UITableViewDelegate,
    MovieMemberMoviesListViewModelDelegate {

    private let MOVIE_CELL_IDENTIFIER = "movieCellView"
    private let movieMemberMoviesListViewModel: MovieMemberMoviesListViewModel
    private let memberId: Int
    private let showMoviesAsCastMember: Bool
    private var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet weak var movieMemberMoviesTableView: UITableView!
    
    init(memberId: Int, showMoviesAsCastMember: Bool, movieMemberMoviesListViewModel: MovieMemberMoviesListViewModel) {
        self.movieMemberMoviesListViewModel = movieMemberMoviesListViewModel
        self.memberId = memberId
        self.showMoviesAsCastMember = showMoviesAsCastMember
        super.init(nibName: ConstantString.screensNibNames.movieMemberMoviesList, bundle: nil)
        self.movieMemberMoviesListViewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startSpinner(activityIndicator: self.activityIndicator)
        self.movieMemberMoviesListViewModel.loadMoviesOfMovieMember(memberId: self.memberId, showMoviesAsCastMember: self.showMoviesAsCastMember)
        title = ConstantString.screensTitles.moviesList
        self.movieMemberMoviesTableView.register(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: MOVIE_CELL_IDENTIFIER)
        self.movieMemberMoviesTableView.delegate = self
        self.movieMemberMoviesTableView.dataSource = self
        
    }
    
    // MARK MovieMemberMoviesListViewModelDelegate methods
    func moviesLoaded() {
        self.movieMemberMoviesTableView.reloadData()
        self.stopSpinner(activityIndicator: self.activityIndicator)
    }
    
    // MARK UITableViewDataSource methods
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.movieMemberMoviesListViewModel.movieMemberMovieInformationViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MOVIE_CELL_IDENTIFIER, for: indexPath) as! MovieTableViewCell
        let movieInformation = movieMemberMoviesListViewModel.movieMemberMovieInformationViewModels[indexPath.row]
        cell.principalImage.image = movieInformation.movieInformationImage
        cell.title.text = movieInformation.movieInformationTitle
        cell.subtitle.text = "As \(movieInformation.movieInformationPerformance)"
        cell.subtitle.textColor = UIColor.gray
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let movieInfo = self.movieMemberMoviesListViewModel.movieMemberMovieInformationViewModels[indexPath.row]
        let movieInformationViewModel = self.movieMemberMoviesListViewModel.getMovieInformationViewModel()
        let vc = MovieInformationViewController(movieId: movieInfo.movieInformationId, movieCover: movieInfo.movieInformationImage, movieInformationViewModel: movieInformationViewModel)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
