import UIKit

protocol MovieMemberMoviesListStorageDelegate: class {
    func moviesLoaded(movieMemberMoviesListStorage: MovieMemberMoviesListStorage)
    func coverLoaded()
}

class MovieMemberMoviesListStorage: ApiConnector {
    weak var delegate: MovieMemberMoviesListStorageDelegate?
    
    var allMovies: [MovieMemberMovieInformation] = []
    var loadMoviesAsMemberOfCast = true
    
    func loadMoviesOfMovieMemberAsMemberOfCast(memberId: Int) {
        self.allMovies.removeAll()
        self.loadMoviesAsMemberOfCast = true
        // Go to extract information
        self.getJSONFromAPI(url: generateUrl(memberId: memberId))
    }
    
    func loadMoviesOfMovieMemberAsMemberOfCrew(memberId: Int) {
        self.allMovies.removeAll()
        self.loadMoviesAsMemberOfCast = false
        // Go to extract information
        self.getJSONFromAPI(url: generateUrl(memberId: memberId))
    }
    
    private func generateUrl(memberId: Int) -> URL{
        let url = "\(ConstantString.appSettings.baseUrl)person/\(memberId)/movie_credits?api_key=\(ConstantString.appSettings.apiKey)"
        print(url)
        return URL(string: url)!
    }
    
    override func extractInformation(dataInJSON: [String : Any]) {
        (self.loadMoviesAsMemberOfCast)
        ? self.extractMoviesAsMemberOfCast(dataInJSON)
        : self.extractMoviesAsMemberOfCrew(dataInJSON)
        
    }
    
    // MARK methods to extract movies as member of a cast
    
    private func extractMoviesAsMemberOfCast(_ json: [String: Any]){
        guard let cast = json["cast"] as? [[String: Any]]
            else {
                print("Error getting movies as member of cast")
                return
        }
        self.extractAllMoviesAsMemberOfCast(cast)
        DispatchQueue.main.async {
            self.delegate?.moviesLoaded(movieMemberMoviesListStorage: self)
        }
    }
    
    private func extractAllMoviesAsMemberOfCast(_ cast: [[String: Any]]) -> Void{
        cast.map({ (movie) in
            let movieInformation = self.extractMovieAsMemberOfCast(movie)
            self.downloadImage(object: movieInformation, url: movieInformation.coverUrl)
            self.allMovies.append(movieInformation)
        })
    }
    
    private func extractMovieAsMemberOfCast(_ movie: [String: Any]) -> MovieMemberMovieInformation{
        guard let id = movie["id"] as? Int,
            let title = movie["title"] as? String,
            let performance = movie["character"] as? String,
            let voteAverage = movie["vote_average"] as? Float
            else{
                print("Error extracting movie as member of cast")
                return MovieMemberMovieInformation()
        }
        guard let coverUrl = movie["poster_path"]
            else{
                return MovieMemberMovieInformation(id: id, title: title, performance: performance, voteAverage: voteAverage, coverUrl: "")
        }
        return MovieMemberMovieInformation(id: id, title: title, performance: performance, voteAverage: voteAverage, coverUrl: "\(ConstantString.appSettings.baseImageUrl)\(coverUrl)")
    }
    
    // MARK methods to extract movies as member of a crew
    
    private func extractMoviesAsMemberOfCrew(_ json: [String: Any]){
        guard let crew = json["crew"] as? [[String: Any]]
            else {
                print("Error getting movies as member of crew")
                return
        }
        self.extractAllMoviesAsMemberOfCrew(crew)
        DispatchQueue.main.async {
            self.delegate?.moviesLoaded(movieMemberMoviesListStorage: self)
        }
    }
    
    private func extractAllMoviesAsMemberOfCrew(_ crew: [[String: Any]]) -> Void{
        crew.map({ (movie) in
            let movieInformation = self.extractMovieAsMemberOfCrew(movie)
            self.downloadImage(object: movieInformation, url: movieInformation.coverUrl)
            self.allMovies.append(movieInformation)
        })
    }
    
    private func extractMovieAsMemberOfCrew(_ movie: [String: Any]) -> MovieMemberMovieInformation{
        guard let id = movie["id"] as? Int,
            let title = movie["title"] as? String,
            let performance = movie["job"] as? String,
            let voteAverage = movie["vote_average"] as? Float
            else{
                print("Error extracting movie as member of cast")
                return MovieMemberMovieInformation()
        }
        guard let coverUrl = movie["poster_path"]
            else{
                return MovieMemberMovieInformation(id: id, title: title, performance: performance, voteAverage: voteAverage, coverUrl: "")
        }
        return MovieMemberMovieInformation(id: id, title: title, performance: performance, voteAverage: voteAverage, coverUrl: "\(ConstantString.appSettings.baseImageUrl)\(coverUrl)")
    }
    
    override func saveImageInObject(object: Any, image: UIImage) {
        let movieInformation = object as! MovieMemberMovieInformation
        movieInformation.cover = image
        DispatchQueue.main.async {
            self.delegate?.coverLoaded()
        }
    }
}
