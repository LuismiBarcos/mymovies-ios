import UIKit

class MovieMemberMovieInformationViewModel {
    let movieMemberMovieInformation: MovieMemberMovieInformation
    
    init(movieMemberMovieInformation: MovieMemberMovieInformation) {
        self.movieMemberMovieInformation = movieMemberMovieInformation
    }
    
    var movieInformationId: Int {
        get {
            return self.movieMemberMovieInformation.id
        }
    }
    
    var movieInformationTitle: String {
        get {
            return self.movieMemberMovieInformation.title
        }
    }
    
    var movieInformationPerformance: String {
        get {
            return self.movieMemberMovieInformation.performance
        }
    }
    
    var movieInformationVoteAverage: Float {
        get {
            return self.movieMemberMovieInformation.voteAverage
        }
    }
    
    var movieInformationImage: UIImage {
        get {
            return self.movieMemberMovieInformation.cover            
        }
    }
}
