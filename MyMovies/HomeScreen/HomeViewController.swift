import UIKit
import UIScrollView_InfiniteScroll

class HomeViewController:
    UIViewController,
    UITableViewDataSource,
    UITableViewDelegate,
    HomeViewModelDelegate {
    
    @IBOutlet weak var moviesTableView: UITableView!
    
    private let MOVIE_CELL_IDENTIFIER = "movieCellView"
    
    private let homeViewModel: HomeViewModel
    
    private var pageOfMoviesToLoad = 1
    private var segmentedStatus = 0
    private var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    
    init(homeViewModel: HomeViewModel) {
        self.homeViewModel = homeViewModel
        super.init(nibName: nil, bundle: nil)
        self.homeViewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = ConstantString.screensTitles.home
        self.moviesTableView.register(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: MOVIE_CELL_IDENTIFIER)
        self.moviesTableView.delegate = self
        self.moviesTableView.dataSource = self
        self.startSpinner(activityIndicator: self.activityIndicator)
        homeViewModel.loadUpcomingMovies(page: self.pageOfMoviesToLoad)
        self.pageOfMoviesToLoad = self.pageOfMoviesToLoad + 1
        self.moviesTableView.addInfiniteScroll { (tableView) in
            // update table view
            self.loadMoreMovies()
            self.pageOfMoviesToLoad = self.pageOfMoviesToLoad + 1
            // finish infinite scroll animation
            tableView.finishInfiniteScroll()
        }
    }
    
    private func loadMoreMovies(){
        switch self.segmentedStatus {
        case 0:
            homeViewModel.loadUpcomingMovies(page: self.pageOfMoviesToLoad)
        case 1:
            homeViewModel.loadTopRatedMovies(page: self.pageOfMoviesToLoad)
        case 2:
            homeViewModel.loadPopularMovies(page: self.pageOfMoviesToLoad)
        default:
            break
        }
    }

    @IBAction func segmentedControl(_ sender: UISegmentedControl) {
        self.pageOfMoviesToLoad = 1
        self.segmentedStatus = sender.selectedSegmentIndex
        switch sender.selectedSegmentIndex {
        case 0:
            homeViewModel.loadUpcomingMovies(page: self.pageOfMoviesToLoad)
        case 1:
            homeViewModel.loadTopRatedMovies(page: self.pageOfMoviesToLoad)
        case 2:
            homeViewModel.loadPopularMovies(page: self.pageOfMoviesToLoad)
        default:
            break
        }
        self.pageOfMoviesToLoad = self.pageOfMoviesToLoad + 1
    }
    
    // MARK HomeViewModelDelegate methods
    
    func homeViewModelDidLoadMovies(_: HomeViewModel) {
        moviesTableView.reloadData()
        self.stopSpinner(activityIndicator: self.activityIndicator)
    }
    
    // MARK UITableViewDataSource methods
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.homeViewModel.movieInfoViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MOVIE_CELL_IDENTIFIER, for: indexPath) as! MovieTableViewCell
        cell.principalImage.image = homeViewModel.movieInfoViewModels[indexPath.row].image
        cell.title.text = homeViewModel.movieInfoViewModels[indexPath.row].title
        let voteAverage = homeViewModel.movieInfoViewModels[indexPath.row].voteAverage
        cell.subtitle.text = "⭐️ \(voteAverage)/10"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let movieInfoViewModel = self.homeViewModel.movieInfoViewModels[indexPath.row]
        let movieInformationViewModel = self.homeViewModel.getMovieInformationViewModel()
        let vc = MovieInformationViewController(movieId: movieInfoViewModel.id, movieCover: movieInfoViewModel.image, movieInformationViewModel: movieInformationViewModel)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

