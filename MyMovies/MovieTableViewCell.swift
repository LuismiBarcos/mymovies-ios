import UIKit

class MovieTableViewCell: UITableViewCell {
    @IBOutlet weak var principalImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
}
