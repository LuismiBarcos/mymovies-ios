import UIKit

class MyMoviesZoneViewController: UIViewController,
    UITableViewDelegate,
    UITableViewDataSource,
    MyMoviesZoneViewModelDelegate
{

    @IBOutlet weak var myMoviesTableView: UITableView!
    
    private let myMoviesZoneViewModel: MyMoviesZoneViewModel
    
    private let MOVIE_CELL_IDENTIFIER = "movieCellView"
    
    var selectedSegmentIndex = 0
    
    init(myMoviesZoneViewModel: MyMoviesZoneViewModel) {
        self.myMoviesZoneViewModel = myMoviesZoneViewModel
        super.init(nibName: ConstantString.screensNibNames.myMoviesZone, bundle: nil)
        self.myMoviesZoneViewModel.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = ConstantString.screensTitles.myMoviesZone
        self.myMoviesTableView.register(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: MOVIE_CELL_IDENTIFIER)
        self.myMoviesTableView.delegate = self
        self.myMoviesTableView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.segementedHandler(selectedSegmentIndex: self.selectedSegmentIndex)
    }
    
    @IBAction func segmentedControl(_ sender: UISegmentedControl) {
        self.segementedHandler(selectedSegmentIndex: sender.selectedSegmentIndex)
    }
    
    private func segementedHandler(selectedSegmentIndex: Int){
        self.selectedSegmentIndex = selectedSegmentIndex
        switch selectedSegmentIndex {
        case 0:
            self.myMoviesZoneViewModel.loadFavoriteMovies()
        case 1:
            self.myMoviesZoneViewModel.loadPendingMovies()
        case 2:
            self.myMoviesZoneViewModel.loadViewedMovies()
        default:
            break
        }
    }
    
    // MARK MyMoviesZoneViewModelDelegate methods
    
    func moviesLoaded() {
        self.myMoviesTableView.reloadData()
    }
    
    // MARK UITableViewDelegate methods
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.myMoviesZoneViewModel.movieInfoViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MOVIE_CELL_IDENTIFIER, for: indexPath) as! MovieTableViewCell
        let movieInfo = self.myMoviesZoneViewModel.movieInfoViewModels[indexPath.row]
        cell.principalImage.image = movieInfo.movieCover
        cell.title.text = movieInfo.movieTitle
        cell.subtitle.text = "⭐️ \(movieInfo.movieAverage)/10"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let movieInformationViewModel = self.myMoviesZoneViewModel.getMovieInformationViewModel()
        let movieInformation = self.myMoviesZoneViewModel.movieInfoViewModels[indexPath.row]
        let vc = MovieInformationViewController(movieId: movieInformation.movieId, movieCover: movieInformation.movieCover, movieInformationViewModel: movieInformationViewModel)
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
