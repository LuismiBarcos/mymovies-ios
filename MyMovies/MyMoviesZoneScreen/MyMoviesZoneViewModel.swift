import UIKit

protocol MyMoviesZoneViewModelDelegate: class {
    func moviesLoaded()
}

class MyMoviesZoneViewModel: MyMoviesZoneStorageDelegate {
    weak var delegate: MyMoviesZoneViewModelDelegate?
    
    var movieInfoViewModels: [MovieInfoViewModel] = []
    
    private let myMoviesZoneStorage: MyMoviesZoneStorage
    
    init(myMoviesZoneStorage: MyMoviesZoneStorage) {
        self.myMoviesZoneStorage = myMoviesZoneStorage
        self.myMoviesZoneStorage.delegate = self
    }
    
    func getMovieInformationViewModel() -> MovieInformationViewModel {
        let movieinformationStorage = MovieinformationStorage()
        return MovieInformationViewModel(movieInformationStorage: movieinformationStorage)
    }
    
    func loadFavoriteMovies(){
        self.myMoviesZoneStorage.loadFavoriteMovies()
    }
    
    func loadViewedMovies(){
        self.myMoviesZoneStorage.loadViewedMovies()
    }
    
    func loadPendingMovies(){
        self.myMoviesZoneStorage.loadPendingMovies()
    }
    
    private func updateMovieInfoViewModels(_ myMoviesZoneStorage: MyMoviesZoneStorage) {
        self.movieInfoViewModels = myMoviesZoneStorage.allMovies.map({ (movie) in
            return MovieInfoViewModel(movieInformation: movie)
        })
    }
    
    // MARK MyMoviesZoneStorageDelegate methods
    
    func moviesLoaded(myMoviesZoneStorage: MyMoviesZoneStorage) {
        self.updateMovieInfoViewModels(myMoviesZoneStorage)
        self.delegate?.moviesLoaded()
    }
}
