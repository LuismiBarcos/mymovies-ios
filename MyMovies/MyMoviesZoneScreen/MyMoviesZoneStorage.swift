import UIKit
import SQLite

protocol MyMoviesZoneStorageDelegate: class {
    func moviesLoaded(myMoviesZoneStorage: MyMoviesZoneStorage)
}

class MyMoviesZoneStorage {
    weak var delegate: MyMoviesZoneStorageDelegate?
    
    var db: Connection?
    var allMovies: [MovieInformation] = []
    
    init() {
        self.makeDBConnection()
    }
    
    private func makeDBConnection() {
        let directory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        do {
            let path = directory[0] + "/db.sqlite"
            db = try Connection(path)
            print("BD abierta en \(path)\n")
        } catch {
            print("Error abriendo BD\n")
        }
        MyMoviesZoneInformation(movieId: 0, conn: db!).createTable()
    }
    
    func loadFavoriteMovies(){
        self.allMovies.removeAll()
        let moviesIds = self.getAllFavoriteMoviesIds()
        self.appendMovies(moviesIds)
    }
    
    func loadViewedMovies(){
        self.allMovies.removeAll()
        let moviesIds = self.getAllViewedMoviesIds()
        self.appendMovies(moviesIds)
    }
    
    func loadPendingMovies(){
        self.allMovies.removeAll()
        let moviesIds = self.getAllPendingMoviesIds()
        self.appendMovies(moviesIds)
    }
    
    private func appendMovies(_ moviesIds: [Int]) {
        for id in moviesIds {
            self.allMovies.append(self.getMovieInformation(id))
        }
        self.delegate?.moviesLoaded(myMoviesZoneStorage: self)
    }
    
    private func getAllFavoriteMoviesIds() -> [Int]{
        return MyMoviesZoneInformation(movieId: 0, conn: db!).getAllFavoriteMoviesIds()
    }
    
    private func getAllViewedMoviesIds() -> [Int]{
        return MyMoviesZoneInformation(movieId: 0, conn: db!).getAllViewedMoviesIds()
    }
    
    private func getAllPendingMoviesIds() -> [Int]{
        return MyMoviesZoneInformation(movieId: 0, conn: db!).getAllPendingMoviesIds()
    }
    
    private func getMovieInformation(_ movieId: Int) -> MovieInformation {
        return MovieInformation(conn: db!, movieId: movieId)
    }
}
